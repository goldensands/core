package com.goldensandsmc.core;

import com.goldensandsmc.core.commands.NicknameCommand;
import com.goldensandsmc.core.commands.RealnameCommand;
import com.goldensandsmc.core.listeners.EnchantmentListener;
import com.goldensandsmc.core.listeners.PlayerListener;
import com.goldensandsmc.core.player.PlayerContainer;
import com.goldensandsmc.core.player.PlayerManager;
import com.goldensandsmc.core.player.data.Nickname;
import com.goldensandsmc.core.player.data.PlayerGeoIP;
import com.goldensandsmc.core.sql.MySQLDBHandler;
import com.goldensandsmc.core.util.CustomEnchantment;
import com.goldensandsmc.core.util.EnchantmentHelper;
import com.goldensandsmc.core.util.JsonConfig;
import com.goldensandsmc.core.util.ReflectionHelper;
import com.goldensandsmc.core.util.Utils;
import com.goldensandsmc.core.util.cache.CachedValue;
import com.google.common.collect.ImmutableList;
import com.shortcircuit.utils.bukkit.command.CommandRegister;
import java.io.File;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.bukkit.FireworkEffect;
import org.bukkit.enchantments.EnchantmentTarget;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * @author ShortCircuit908
 *         Created on 5/17/2017.
 */
public class SandcastleCore extends JavaPlugin {
	public static final CustomEnchantment ENCHANTMENT_PARTICLES = new CustomEnchantment(128, "particles", EnchantmentListener.PARTICLE_EFFECTS.size(), 1, EnchantmentTarget.ARMOR_FEET);

	private static SandcastleCore instance;
	private final JsonConfig config;
	private MySQLDBHandler db_handler;
	private boolean db_initialized = false;
	private int server_id;

	static {
		/* Enchantments */
		EnchantmentHelper.registerCustomEnchantment(ENCHANTMENT_PARTICLES);
	}

	public SandcastleCore() {
		SandcastleCore.instance = this;
		this.config = new JsonConfig(this, new File(getDataFolder() + File.separator + "config.json"), "config.json");
		this.config.loadConfig();
	}

	public static SandcastleCore getInstance() {
		return instance;
	}

	@Override
	public void onEnable() {
		if (!(db_initialized = initializeDatabase())) {
			getLogger().severe("Could not initialize database connection!");
			getServer().getPluginManager().disablePlugin(this);
			config.saveDefaultConfig();
			return;
		}
		db_handler.performUpgrades();

		/* Get server ID */
		try {
			server_id = getOrCreateServerId();
			if (server_id < 0) {
				throw new Exception();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			getLogger().severe("There was a problem getting this server's ID!");
			getLogger().severe("Make sure the server-id property is properly configured with a unique ID in the server's server.properties");
			getServer().getPluginManager().disablePlugin(this);
			return;
		}

		/* Listeners */
		getServer().getPluginManager().registerEvents(new PlayerListener(), this);
		getServer().getPluginManager().registerEvents(new PlayerManager(), this);
		getServer().getPluginManager().registerEvents(new EnchantmentListener(), this);

		/* Commands */
		CommandRegister.register(new NicknameCommand(this));
		CommandRegister.register(new RealnameCommand(this));

		/* Data Containers */
		PlayerManager.registerDataContainer(Nickname.class);
		PlayerManager.registerDataContainer(PlayerGeoIP.class);

		try {
			Class<FireworkEffect> c = FireworkEffect.class;
			Field colors_field = ReflectionHelper.getField(c, "colors");
			System.out.println("ImmutableList.class = " + ImmutableList.class);
			System.out.println("colors_field.getType() = " + colors_field.getType());
		}
		catch (ReflectiveOperationException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onDisable() {
		for (CachedValue<PlayerContainer> cached_player : PlayerManager.getAllCachedPlayers()) {
			cached_player.setCanExpire(true);
			cached_player.expire();
		}
		PlayerManager.forceCacheCleanup();
		if (db_initialized) {
			try {
				updateServerInformation();
			}
			catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	private boolean initializeDatabase() {
		getLogger().info("Initializing MySQL database...");
		if (db_handler != null) {
			getLogger().warning("Error: Database already initialized");
			return false;
		}
		String db_name = config.getNode("database.name", String.class, null);
		if (db_name == null) {
			getLogger().warning("Configuration entry \"database.name\" must be set");
			return false;
		}
		String db_host = config.getNode("database.host", String.class, null);
		if (db_host == null) {
			getLogger().warning("Configuration entry \"database.host\" must be set");
			return false;
		}
		Integer db_port = config.getNode("database.port", Integer.class, null);
		if (db_port == null || db_port < 1 || db_port > 65535) {
			getLogger().warning("Configuration entry \"database.port\" must be set to a valid port number (1-65535)");
			return false;
		}
		String db_username = config.getNode("database.username", String.class, null);
		if (db_username == null) {
			getLogger().warning("Configuration entry \"database.username\" must be set");
			return false;
		}
		String db_password = config.getNode("database.password", String.class, null);
		if (db_password == null) {
			getLogger().warning("Configuration entry \"database.password\" must be set");
			return false;
		}
		try {
			db_handler = new MySQLDBHandler(db_username, db_password, db_host, db_port, db_name);
		}
		catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	private int getOrCreateServerId() throws SQLException {
		try (Connection connection = db_handler.getConnection()) {
			try (PreparedStatement statement = connection.prepareStatement("SELECT `id` FROM `servers` WHERE `server_id`=?")) {
				statement.setString(1, getServer().getServerId());
				try (ResultSet result = statement.executeQuery()) {
					if (result.next()) {
						return result.getInt(1);
					}
				}
			}
			try (PreparedStatement statement = connection.prepareStatement("INSERT INTO `servers` (`server_id`, `name`, `host`, `port`) VALUES (?, ?, ?, ?)")) {
				statement.setString(1, getServer().getServerId());
				statement.setString(2, getServer().getServerName());
				statement.setString(3, getServer().getIp() == null || getServer().getIp().trim().isEmpty() ? "localhost" : getServer().getIp());
				statement.setInt(4, getServer().getPort());
				statement.execute();
			}
			return Utils.getLastInsertId(connection);
		}
	}

	private void updateServerInformation() throws SQLException {
		try (Connection connection = db_handler.getConnection()) {
			try (PreparedStatement statement = connection.prepareStatement("UPDATE `servers` SET `server_id`=?, `name`=?, `host`=?, `port`=? WHERE `id`=?")) {
				statement.setString(1, getServer().getServerId());
				statement.setString(2, getServer().getServerName());
				statement.setString(3, getServer().getIp() == null || getServer().getIp().trim().isEmpty() ? "localhost" : getServer().getIp());
				statement.setInt(4, getServer().getPort());
			}
		}
	}

	public int getServerId() {
		return server_id;
	}

	public MySQLDBHandler getDBHandler() {
		return db_handler;
	}

	public JsonConfig getJsonConfig() {
		return config;
	}
}
