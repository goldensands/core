package com.goldensandsmc.core.sql;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * @author ShortCircuit908
 *         Created on 3/7/2017.
 */
public abstract class DBUpgrader implements Comparable<DBUpgrader> {
	private int priority = 0;

	public DBUpgrader(int priority){
		this.priority = priority;
	}

	public abstract boolean requiresUpgrade(DataSource data_source, String dbname) throws SQLException;

	public abstract void upgrade(DataSource data_source, String dbname) throws SQLException;

	public abstract String[] getNotes();

	protected void executeSqlBatch(Connection connection, String batch) throws SQLException {
		String[] raw_statements = batch.split(";");
		for (String raw_statement : raw_statements) {
			if (raw_statement.trim().isEmpty()) {
				continue;
			}
			try (PreparedStatement statement = connection.prepareStatement(raw_statement.trim())) {
				statement.execute();
			}
		}
	}

	@Override
	public int compareTo(DBUpgrader other){
		return other == null ? 1 : Integer.compare(priority, other.priority);
	}
}
