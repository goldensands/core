package com.goldensandsmc.core.sql;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * @author ShortCircuit908
 */
public class MySQLDBHandler {
	private final String db_name;
	private final LinkedList<DBUpgrader> upgraders = new LinkedList<>();
	private final HikariDataSource connection_pool;
	private AtomicBoolean performing_upgrades = new AtomicBoolean(false);

	public MySQLDBHandler(String properties_file_name) throws SQLException {
		this(new HikariConfig(properties_file_name));
	}

	public MySQLDBHandler(Properties properties) throws SQLException {
		this(new HikariConfig(properties));
	}

	public MySQLDBHandler(HikariConfig config) throws SQLException{
		this.connection_pool = new HikariDataSource(config);
		this.db_name = config.getJdbcUrl().substring(config.getJdbcUrl().lastIndexOf('/') + 1);
	}

	public MySQLDBHandler(String database_username, String database_password, String database_host, int database_port, String db_name) throws SQLException{
		String database_url = "jdbc:mysql://" + database_host + ":" + database_port + "/" + db_name;
		HikariConfig config = new HikariConfig();
		config.setMaximumPoolSize(8);
		config.setDataSourceClassName("com.mysql.jdbc.jdbc2.optional.MysqlDataSource");
		config.addDataSourceProperty("url", database_url);
		config.addDataSourceProperty("user", database_username);
		config.addDataSourceProperty("password", database_password);
		config.setConnectionTimeout(3000);
		config.addDataSourceProperty("cachePrepStmts", true);
		config.addDataSourceProperty("prepStmtCacheSize", 250);
		config.addDataSourceProperty("prepStmtCacheSqlLimit", 2048);
		config.addDataSourceProperty("useServerPrepStmts", true);
		this.connection_pool = new HikariDataSource(config);
		this.db_name = db_name;
	}

	public void addDatabaseUpgraders(DBUpgrader... upgraders) {
		addDatabaseUpgraders(Arrays.asList(upgraders));
	}

	public void addDatabaseUpgraders(Collection<? extends DBUpgrader> upgraders) {
		this.upgraders.addAll(upgraders);
		Collections.sort(this.upgraders);
	}

	public void performUpgrades() {
		if (performing_upgrades.compareAndSet(false, true)) {
			for (DBUpgrader upgrader : upgraders) {
				try {
					if (upgrader.requiresUpgrade(connection_pool, db_name)) {
						upgrader.upgrade(connection_pool, db_name);
					}
				}
				catch (SQLException e) {
					e.printStackTrace();
				}
			}
			performing_upgrades.set(false);
		}
	}

	public Connection getConnection() throws SQLException {
		return connection_pool.getConnection();
	}
}
