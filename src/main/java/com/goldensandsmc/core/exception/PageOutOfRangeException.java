package com.goldensandsmc.core.exception;

import com.shortcircuit.utils.bukkit.command.exceptions.InvalidArgumentException;

/**
 * @author ShortCircuit908
 *         Created on 6/7/2017.
 */
public class PageOutOfRangeException extends InvalidArgumentException {
	public PageOutOfRangeException(int max_page){
		super("Page out of range (max " + max_page + ")");
	}
}
