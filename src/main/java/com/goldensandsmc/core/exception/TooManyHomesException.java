package com.goldensandsmc.core.exception;

import com.shortcircuit.utils.bukkit.command.exceptions.CommandException;

/**
 * @author ShortCircuit908
 *         Created on 6/7/2017.
 */
public class TooManyHomesException extends CommandException {
	public TooManyHomesException(int maximum) {
		super("You have reached the maximum of " + maximum + " home(s)");
	}
}
