package com.goldensandsmc.core.exception;

import com.shortcircuit.utils.bukkit.command.exceptions.InvalidArgumentException;

import java.net.InetAddress;
import java.util.UUID;

/**
 * @author ShortCircuit908
 *         Created on 4/17/2017.
 */
public class NoSuchPlayerException extends InvalidArgumentException {
	public NoSuchPlayerException(int id) {
		super("No player with ID: " + id);
	}

	public NoSuchPlayerException(UUID uuid) {
		super("No player with UUID: " + uuid.toString());
	}

	public NoSuchPlayerException(String name, boolean nickname, boolean partial) {
		super("No player with " + (nickname ? "nickname" : "username") + (partial ? " containing" : "") + ": " + name);
	}

	public NoSuchPlayerException(InetAddress address) {
		super("No player with address: " + address);
	}
}
