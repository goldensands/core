package com.goldensandsmc.core.exception;

import com.shortcircuit.utils.bukkit.command.exceptions.InvalidArgumentException;

/**
 * @author ShortCircuit908
 *         Created on 6/7/2017.
 */
public class NoSuchWorldException extends InvalidArgumentException {
	public NoSuchWorldException(){
		super("World does not exist");
	}

	public NoSuchWorldException(String world){
		super("World \"" + world + "\" does not exist");
	}
}
