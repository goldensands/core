package com.goldensandsmc.core.exception;

import com.shortcircuit.utils.bukkit.command.exceptions.InvalidArgumentException;

/**
 * @author ShortCircuit908
 *         Created on 6/7/2017.
 */
public class ContentTooLongException extends InvalidArgumentException {
	public ContentTooLongException(int max_chars){
		super("The content is too long (max " + max_chars + " characters)");
	}
}
