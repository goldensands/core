package com.goldensandsmc.core.exception;

import com.shortcircuit.utils.bukkit.command.exceptions.InvalidArgumentException;

/**
 * @author ShortCircuit908
 *         Created on 6/7/2017.
 */
public class NoSuchWarpException extends InvalidArgumentException {
	public NoSuchWarpException(String name) {
		super("Warp \"" + name + "\" does not exist");
	}
}
