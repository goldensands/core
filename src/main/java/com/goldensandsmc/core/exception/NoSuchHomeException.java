package com.goldensandsmc.core.exception;

import com.shortcircuit.utils.bukkit.command.exceptions.InvalidArgumentException;

/**
 * @author ShortCircuit908
 *         Created on 6/7/2017.
 */
public class NoSuchHomeException extends InvalidArgumentException {
	public NoSuchHomeException(String name){
		super("Home \"" + name + "\" does not exist");
	}
}
