package com.goldensandsmc.core.exception;

import com.goldensandsmc.core.util.Utils;
import com.shortcircuit.utils.bukkit.command.exceptions.CommandException;
import org.bukkit.ChatColor;

/**
 * @author ShortCircuit908
 *         Created on 6/10/2017.
 */
public class CooldownException extends CommandException {
	public CooldownException(long cooldown_millis) {
		super("You may use this command in " + Utils.formatTime(cooldown_millis, ChatColor.RED, ChatColor.GOLD));
	}
}
