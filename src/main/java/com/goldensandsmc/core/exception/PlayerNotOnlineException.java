package com.goldensandsmc.core.exception;

import com.shortcircuit.utils.bukkit.command.exceptions.InvalidArgumentException;

/**
 * @author ShortCircuit908
 *         Created on 4/17/2017.
 */
public class PlayerNotOnlineException extends InvalidArgumentException {
	public PlayerNotOnlineException(){
		super("The specified player is not online");
	}
}
