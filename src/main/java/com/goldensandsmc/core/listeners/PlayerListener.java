package com.goldensandsmc.core.listeners;

import com.goldensandsmc.core.exception.NoSuchPlayerException;
import com.goldensandsmc.core.player.PlayerContainer;
import com.goldensandsmc.core.player.PlayerManager;
import com.goldensandsmc.core.player.data.Nickname;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerJoinEvent;

import java.sql.SQLException;

/**
 * @author ShortCircuit908
 *         Created on 5/17/2017.
 */
public class PlayerListener implements Listener {
	@EventHandler
	public void onJoin(final PlayerJoinEvent event) {
		try {
			PlayerContainer container = PlayerManager.getOrCreate(event.getPlayer()).getValue();
			// Apply nickname
			container.getDataContainer(Nickname.class).apply(event.getPlayer());
		}
		catch (NoSuchPlayerException | SQLException e) {
			e.printStackTrace();
		}
	}

	@EventHandler
	public void godMode(final EntityDamageEvent event) {
		if (event.isCancelled() || !(event.getEntity() instanceof Player)) {
			return;
		}
		Player player = (Player) event.getEntity();
		boolean godmode = false;
		try {
			godmode = PlayerManager.getOrCreate(player.getUniqueId()).getValue().isGodmode();
		}
		catch (NoSuchPlayerException | SQLException e) {
			e.printStackTrace();
		}
		if (godmode) {
			event.setCancelled(true);
		}
	}

	@EventHandler
	public void godModePvp(final EntityDamageByEntityEvent event) {
		if (event.isCancelled() || !(event.getEntity() instanceof Player) || !(event.getDamager() instanceof Player)) {
			return;
		}
		boolean godmode = false;
		try {
			godmode = PlayerManager.getOrCreate(event.getDamager().getUniqueId()).getValue().isGodmode();
		}
		catch (NoSuchPlayerException | SQLException e) {
			e.printStackTrace();
		}
		if (godmode && !event.getDamager().hasPermission("sandcastle.core.god.pvp")) {
			event.getDamager().sendMessage(ChatColor.RED + "You may not hurt other players while in god mode");
			event.setCancelled(true);
		}
	}
}
