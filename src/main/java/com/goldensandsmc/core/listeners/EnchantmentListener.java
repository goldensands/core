package com.goldensandsmc.core.listeners;

import com.goldensandsmc.core.SandcastleCore;
import com.goldensandsmc.core.util.Utils;
import com.shortcircuit.utils.bukkit.PlayerUtils;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.block.BlockFace;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionType;

import java.util.ArrayList;

/**
 * @author ShortCircuit908
 *         Created on 6/8/2017.
 */
public class EnchantmentListener implements Listener {
	public static final ArrayList<ParticleEffect> PARTICLE_EFFECTS = new ArrayList<>();

	static {
		PARTICLE_EFFECTS.add(new ParticleEffect<>(Effect.SMOKE, BlockFace.DOWN));
		PARTICLE_EFFECTS.add(new ParticleEffect<>(Effect.ENDER_SIGNAL, null, true));
		PARTICLE_EFFECTS.add(new ParticleEffect<>(Effect.MOBSPAWNER_FLAMES));
		PARTICLE_EFFECTS.add(new ParticleEffect<>(Effect.FIREWORKS_SPARK));
		PARTICLE_EFFECTS.add(new ParticleEffect<>(Effect.CRIT)); // 5
		PARTICLE_EFFECTS.add(new ParticleEffect<>(Effect.MAGIC_CRIT));
		PARTICLE_EFFECTS.add(new ParticleEffect<>(Effect.POTION_SWIRL, null, 5));
		PARTICLE_EFFECTS.add(new ParticleEffect<>(Effect.POTION_SWIRL_TRANSPARENT, null, 5));
		PARTICLE_EFFECTS.add(new ParticleEffect<>(Effect.SPELL));
		PARTICLE_EFFECTS.add(new ParticleEffect<>(Effect.INSTANT_SPELL)); // 10
		PARTICLE_EFFECTS.add(new ParticleEffect<>(Effect.WITCH_MAGIC));
		PARTICLE_EFFECTS.add(new ParticleEffect<>(Effect.NOTE));
		PARTICLE_EFFECTS.add(new ParticleEffect<>(Effect.PORTAL, null, 10));
		PARTICLE_EFFECTS.add(new ParticleEffect<>(Effect.FLYING_GLYPH));
		PARTICLE_EFFECTS.add(new ParticleEffect<>(Effect.FLAME)); // 15
		PARTICLE_EFFECTS.add(new ParticleEffect<>(Effect.LAVA_POP));
		PARTICLE_EFFECTS.add(new ParticleEffect<>(Effect.FOOTSTEP));
		PARTICLE_EFFECTS.add(new ParticleEffect<>(Effect.SPLASH));
		PARTICLE_EFFECTS.add(new ParticleEffect<>(Effect.PARTICLE_SMOKE));
		PARTICLE_EFFECTS.add(new ParticleEffect<>(Effect.EXPLOSION_HUGE)); // 20
		PARTICLE_EFFECTS.add(new ParticleEffect<>(Effect.EXPLOSION_LARGE));
		PARTICLE_EFFECTS.add(new ParticleEffect<>(Effect.EXPLOSION));
		PARTICLE_EFFECTS.add(new ParticleEffect<>(Effect.VOID_FOG, null, 20));
		PARTICLE_EFFECTS.add(new ParticleEffect<>(Effect.SMALL_SMOKE, null, 20));
		PARTICLE_EFFECTS.add(new ParticleEffect<>(Effect.CLOUD)); // 25
		PARTICLE_EFFECTS.add(new ParticleEffect<>(Effect.COLOURED_DUST, null, 5));
		PARTICLE_EFFECTS.add(new ParticleEffect<>(Effect.SNOWBALL_BREAK));
		PARTICLE_EFFECTS.add(new ParticleEffect<>(Effect.WATERDRIP, null, true));
		PARTICLE_EFFECTS.add(new ParticleEffect<>(Effect.LAVADRIP, null, true));
		PARTICLE_EFFECTS.add(new ParticleEffect<>(Effect.SNOW_SHOVEL)); // 30
		PARTICLE_EFFECTS.add(new ParticleEffect<>(Effect.SLIME));
		PARTICLE_EFFECTS.add(new ParticleEffect<>(Effect.HEART));
		PARTICLE_EFFECTS.add(new ParticleEffect<>(Effect.VILLAGER_THUNDERCLOUD));
		PARTICLE_EFFECTS.add(new ParticleEffect<>(Effect.HAPPY_VILLAGER));
		PARTICLE_EFFECTS.add(new ParticleEffect<>(Effect.LARGE_SMOKE)); // 35
		for (PotionType type : PotionType.values()) {
			PARTICLE_EFFECTS.add(new ParticleEffect<>(Effect.POTION_BREAK, new Potion(type), true));
		}
	}


	@EventHandler
	public void particles(final PlayerMoveEvent event) {
		if (event.getPlayer().isSneaking() || !event.getFrom().getWorld().equals(event.getTo().getWorld())) {
			return;
		}
		double distance_squared = event.getFrom().distanceSquared(event.getTo());
		if (distance_squared < 0.03) {
			return;
		}
		ItemStack boots = event.getPlayer().getInventory().getBoots();
		if (boots == null || !boots.containsEnchantment(SandcastleCore.ENCHANTMENT_PARTICLES)) {
			return;
		}
		int effect_index = boots.getEnchantmentLevel(SandcastleCore.ENCHANTMENT_PARTICLES) - 1;
		if (effect_index < 0 || effect_index >= PARTICLE_EFFECTS.size()) {
			effect_index = 0;
		}
		ParticleEffect<?> effect = PARTICLE_EFFECTS.get(effect_index);
		for (int i = 0; i < effect.num_repetitions; i++) {
			double offset_x = effect.random_immune ? 0 : Utils.RANDOM.nextDouble() - 0.5;
			double offset_z = effect.random_immune ? 0 : Utils.RANDOM.nextDouble() - 0.5;
			double offset_y = 0.25;
			PARTICLE_EFFECTS.get(effect_index).playEffect(event.getTo().add(offset_x, offset_y, offset_z));
		}
	}

	public static class ParticleEffect<T> {
		private final boolean random_immune;
		private final int num_repetitions;
		private final Effect effect;
		private final T data;

		private ParticleEffect(Effect effect) {
			this(effect, null, false, 1);
		}

		private ParticleEffect(Effect effect, T data) {
			this(effect, data, false, 1);
		}

		private ParticleEffect(Effect effect, T data, boolean random_immune) {
			this(effect, data, random_immune, 1);
		}

		private ParticleEffect(Effect effect, T data, int num_repetitions) {
			this(effect, data, false, num_repetitions);
		}

		private ParticleEffect(Effect effect, T data, boolean random_immune, int num_repetitions) {
			this.effect = effect;
			this.data = data;
			this.random_immune = random_immune;
			this.num_repetitions = num_repetitions;
		}

		@SuppressWarnings("unchecked")
		public void playEffect(Location location) {
			T used_data = data;
			if (effect == Effect.SMOKE) {
				used_data = (T) PlayerUtils.getCardinalDirection(location);
			}
			location.getWorld().playEffect(location, effect, used_data);
		}
	}
}
