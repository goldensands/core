package com.goldensandsmc.core.player.data;

import com.goldensandsmc.core.SandcastleCore;
import com.goldensandsmc.core.player.PlayerDataContainer;
import com.goldensandsmc.core.util.Utils;
import com.google.common.collect.ImmutableMap;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.sql.*;
import java.util.Date;
import java.util.Map;
import java.util.UUID;

/**
 * @author ShortCircuit908
 *         Created on 6/5/2017.
 */
public class Nickname extends PlayerDataContainer {
	private String nickname;
	private Date changed_at;

	public Nickname(UUID player_uuid) {
		super(player_uuid);
	}

	public String getNickname() {
		return nickname;
	}

	public Nickname setNickname(String nickname) throws SQLException {
		this.nickname = nickname;
		this.changed_at = new Date();
		save();
		return this;
	}

	public Date getChangedAt() {
		return changed_at;
	}

	public boolean canChange() {
		return canChange(new Date(), changed_at);
	}

	private static boolean canChange(Date now, Date changed_at) {
		if (changed_at == null) {
			return true;
		}
		return now.getTime() - changed_at.getTime() >= SandcastleCore.getInstance().getJsonConfig().getNode("nicknames.cooldown_days", long.class, 5) * 1000 * 60 * 60 * 24;
	}

	public static Object[] nicknameInUse(String nickname) throws SQLException {
		try (Connection connection = SandcastleCore.getInstance().getDBHandler().getConnection()) {
			try (PreparedStatement statement = connection.prepareStatement("SELECT HEX(`player_uuid`), `nickname`, `changed_at` FROM `nicknames`")) {
				try (ResultSet result = statement.executeQuery()) {
					String used_name;
					UUID owner_uuid;
					Date used_changed_at;
					Date now = new Date();
					while (result.next()) {
						owner_uuid = Utils.uuidFromString(result.getString(1));
						used_name = result.getString(2);
						used_changed_at = result.getTimestamp(3);
						if (Utils.isSimilar(nickname, used_name) && !canChange(now, used_changed_at)) {
							return new Object[]{true, owner_uuid};
						}
					}
				}
			}
		}
		return new Object[]{false};
	}

	@Override
	public void load(Connection connection) throws SQLException {
		try (PreparedStatement statement = connection.prepareStatement("SELECT `nickname`, `changed_at` FROM `nicknames` WHERE `player_uuid`=UNHEX(?)")) {
			statement.setString(1, Utils.stringFromUuid(this.getPlayerUuid()));
			try (ResultSet result = statement.executeQuery()) {
				if (result.next()) {
					this.nickname = result.getString(1);
					this.changed_at = result.getTimestamp(2);
				}
			}
		}
	}

	@Override
	public void save(Connection connection) throws SQLException {
		if (this.nickname == null) {
			try (PreparedStatement statement = connection.prepareStatement("DELETE FROM `nicknames` WHERE `player_uuid`=UNHEX(?)")) {
				statement.setString(1, Utils.stringFromUuid(this.getPlayerUuid()));
				statement.execute();
			}
			return;
		}
		try (PreparedStatement statement = connection.prepareStatement("REPLACE INTO `nicknames` (`player_uuid`, `nickname`, `changed_at`) VALUES (UNHEX(?), ?, ?)")) {
			statement.setString(1, Utils.stringFromUuid(this.getPlayerUuid()));
			statement.setString(2, this.nickname);
			statement.setTimestamp(3, new Timestamp(this.changed_at.getTime()));
			statement.execute();
		}
	}

	@Override
	public void apply(Player player) {
		if (getNickname() == null) {
			player.setDisplayName(player.getName());
			return;
		}
		player.setDisplayName("~" + ChatColor.translateAlternateColorCodes('&', getNickname()));
	}

	@Override
	public Map<String, Object> getMapping() {
		return ImmutableMap.of("nickname", getNickname());
	}
}
