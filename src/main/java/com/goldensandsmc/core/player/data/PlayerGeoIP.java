package com.goldensandsmc.core.player.data;

import com.goldensandsmc.core.SandcastleCore;
import com.goldensandsmc.core.player.TransientPlayerDataContainer;
import com.goldensandsmc.core.util.GeoIP;
import com.google.common.collect.ImmutableMap;
import java.io.IOException;
import java.util.Map;
import java.util.UUID;
import org.bukkit.entity.Player;

/**
 * @author ShortCircuit908
 *         Created on 6/29/2017.
 */
public class PlayerGeoIP extends TransientPlayerDataContainer {
	private GeoIP geoip;

	public PlayerGeoIP(UUID player_uuid) {
		super(player_uuid);
	}

	@Override
	public void apply(final Player player) {
		player.getServer().getScheduler().scheduleAsyncDelayedTask(SandcastleCore.getInstance(), new Runnable() {
			@Override
			public void run() {
				try {
					PlayerGeoIP.this.geoip = GeoIP.getGeoIPInformation(player.getAddress().getAddress());
				}
				catch (IOException e) {
					e.printStackTrace();
				}
			}
		});
	}

	@Override
	public Map<String, Object> getMapping() {
		return ImmutableMap.of("geoip", geoip);
	}

	public GeoIP getGeoIP() {
		return geoip;
	}
}
