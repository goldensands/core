package com.goldensandsmc.core.player;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.UUID;

/**
 * @author ShortCircuit908
 *         Created on 6/27/2017.
 */
public abstract class TransientPlayerDataContainer extends PlayerDataContainer {

	public TransientPlayerDataContainer(UUID player_uuid) {
		super(player_uuid);
	}

	@Override
	public void load(Connection connection) throws SQLException {

	}

	@Override
	public void save(Connection connection) throws SQLException {

	}
}
