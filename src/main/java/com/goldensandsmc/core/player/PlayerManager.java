package com.goldensandsmc.core.player;

import com.goldensandsmc.core.SandcastleCore;
import com.goldensandsmc.core.exception.NoSuchPlayerException;
import com.goldensandsmc.core.exception.PlayerNotOnlineException;
import com.goldensandsmc.core.player.data.Nickname;
import com.goldensandsmc.core.util.Utils;
import com.goldensandsmc.core.util.cache.Cache;
import com.goldensandsmc.core.util.cache.CacheMap;
import com.goldensandsmc.core.util.cache.CachedValue;
import com.goldensandsmc.core.util.cache.ExpiryType;
import org.bukkit.OfflinePlayer;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.lang.reflect.Constructor;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * @author ShortCircuit908
 *         Created on 4/17/2017.
 */
public final class PlayerManager implements Listener {
	private static final HashSet<Constructor<? extends PlayerDataContainer>> data_constructors = new HashSet<>();
	private static final CacheMap<UUID, PlayerContainer> players = new CacheMap<>(5, TimeUnit.MINUTES, ExpiryType.SINCE_ACCESSED);

	static {
		players.setExpireTask(new Cache.ExpireTask<PlayerContainer>() {
			@Override
			public void onValueExpired(CachedValue<PlayerContainer> value) {
				try {
					value.getValueSneakily().save();
				}
				catch (SQLException e) {
					e.printStackTrace();
				}
			}
		});
	}

	public PlayerManager() {

	}

	protected static HashSet<Constructor<? extends PlayerDataContainer>> getDataConstructors() {
		return data_constructors;
	}

	public static void registerDataContainer(Class<? extends PlayerDataContainer> data_class) {
		try {
			Constructor<? extends PlayerDataContainer> constructor = data_class.getDeclaredConstructor(UUID.class);
			constructor.setAccessible(true);
			data_constructors.add(constructor);
		}
		catch (NoSuchMethodException e) {
			throw new IllegalArgumentException(data_class + " does not declare a constructor with args (java.util.UUID)");
		}
	}

	public static void forceCacheCleanup() {
		players.expireEntries();
	}

	public static LinkedList<CachedValue<PlayerContainer>> getOnlinePlayers() {
		LinkedList<CachedValue<PlayerContainer>> list = new LinkedList<>();
		for (CachedValue<PlayerContainer> player_container : players.getValues()) {
			if (player_container.getValueSneakily().isOnline()) {
				list.add(player_container);
			}
		}
		return list;
	}

	public static LinkedList<CachedValue<PlayerContainer>> getAllCachedPlayers() {
		return new LinkedList<>(players.getValues());
	}

	public static CachedValue<PlayerContainer> getOrCreate(OfflinePlayer player) throws NoSuchPlayerException, SQLException {
		return getOrCreate(player.getUniqueId());
	}

	public static CachedValue<PlayerContainer> getOrCreate(UUID uuid) throws NoSuchPlayerException, SQLException {
		if (players.containsKey(uuid)) {
			CachedValue<PlayerContainer> value = players.get(uuid);
			PlayerContainer container = value.getValueSneakily();
			if (!container.isLoaded()) {
				container.load();
			}
			if (container.isOnline()) {
				try {
					container.gatherData(container.getPlayer());
				}
				catch (PlayerNotOnlineException e) {
					e.printStackTrace();
				}
			}
			return players.get(uuid);
		}
		PlayerContainer container = new PlayerContainer(uuid);
		if (!container.exists()) {
			throw new NoSuchPlayerException(uuid);
		}
		container.load();
		if (container.isOnline()) {
			try {
				container.gatherData(container.getPlayer());
			}
			catch (PlayerNotOnlineException e) {
				e.printStackTrace();
			}
		}
		return players.put(uuid, container);
	}

	public static CachedValue<PlayerContainer> getOrCreateByUsername(String name) throws NoSuchPlayerException, SQLException {
		for (CachedValue<PlayerContainer> cached : players.getValues()) {
			PlayerContainer container = cached.getValueSneakily();
			if (name.equalsIgnoreCase(container.getName())) {
				if (!container.isLoaded()) {
					container.load();
				}
				if (container.isOnline()) {
					try {
						container.gatherData(container.getPlayer());
					}
					catch (PlayerNotOnlineException e) {
						e.printStackTrace();
					}
				}
				return cached;
			}
		}
		PlayerContainer container = new PlayerContainer(name);
		if (!container.exists()) {
			throw new NoSuchPlayerException(name, false, false);
		}
		container.load();
		if (container.isOnline()) {
			try {
				container.gatherData(container.getPlayer());
			}
			catch (PlayerNotOnlineException e) {
				e.printStackTrace();
			}
		}
		return players.put(container.getUuid(), container);
	}

	public static CachedValue<PlayerContainer> getOrCreateByUsernamePartial(String part) throws NoSuchPlayerException, SQLException {
		for (CachedValue<PlayerContainer> cached : players.getValues()) {
			PlayerContainer container = cached.getValueSneakily();
			if (container.getName().toLowerCase().contains(part)) {
				if (!container.isLoaded()) {
					container.load();
				}
				if (container.isOnline()) {
					try {
						container.gatherData(container.getPlayer());
					}
					catch (PlayerNotOnlineException e) {
						e.printStackTrace();
					}
				}
				return cached;
			}
		}
		try (Connection connection = SandcastleCore.getInstance().getDBHandler().getConnection()) {
			try (PreparedStatement statement = connection.prepareStatement("SELECT HEX(`uuid`) FROM `players` WHERE LOWER(`name`) LIKE CONCAT('%', ?, '%') LIMIT 1")) {
				statement.setString(1, part.toLowerCase());
				try (ResultSet result = statement.executeQuery()) {
					if (!result.next()) {
						throw new NoSuchPlayerException(part, false, true);
					}
					return getOrCreate(Utils.uuidFromString(result.getString(1)));
				}
			}
		}
	}

	public static CachedValue<PlayerContainer> getOrCreateByNickname(String nickname) throws NoSuchPlayerException, SQLException {
		if (nickname.startsWith("~")) {
			nickname = nickname.substring(1);
		}
		for (CachedValue<PlayerContainer> cached : players.getValues()) {
			PlayerContainer container = cached.getValueSneakily();
			Nickname nickname_container = container.getDataContainer(Nickname.class);
			if (nickname_container.getNickname() != null && nickname_container.getNickname().equalsIgnoreCase(nickname)) {
				if (!container.isLoaded()) {
					container.load();
				}
				if (container.isOnline()) {
					try {
						container.gatherData(container.getPlayer());
					}
					catch (PlayerNotOnlineException e) {
						e.printStackTrace();
					}
				}
				return cached;
			}
		}
		try (Connection connection = SandcastleCore.getInstance().getDBHandler().getConnection()) {
			try (PreparedStatement statement = connection.prepareStatement("SELECT HEX(`player_uuid`) FROM `nicknames` WHERE LOWER(`nickname`)=?")) {
				statement.setString(1, nickname.toLowerCase());
				try (ResultSet result = statement.executeQuery()) {
					if (result.next()) {
						return getOrCreate(Utils.uuidFromString(result.getString(1)));
					}
					else {
						throw new NoSuchPlayerException(nickname, true, false);
					}
				}
			}
		}
	}

	public static CachedValue<PlayerContainer> getOrCreateByNicknamePartial(String part) throws NoSuchPlayerException, SQLException {
		if (part.startsWith("~")) {
			part = part.substring(1);
		}
		for (CachedValue<PlayerContainer> cached : players.getValues()) {
			PlayerContainer container = cached.getValueSneakily();
			Nickname nickname_container = container.getDataContainer(Nickname.class);
			if (nickname_container.getNickname() != null && nickname_container.getNickname().contains(part)) {
				if (!container.isLoaded()) {
					container.load();
				}
				if (container.isOnline()) {
					try {
						container.gatherData(container.getPlayer());
					}
					catch (PlayerNotOnlineException e) {
						e.printStackTrace();
					}
				}
				return cached;
			}
		}
		try (Connection connection = SandcastleCore.getInstance().getDBHandler().getConnection()) {
			try (PreparedStatement statement = connection.prepareStatement("SELECT HEX(`player_uuid`) FROM `nicknames` WHERE LOWER(`nickname`) LIKE CONCAT('%', ?, '%') LIMIT 1")) {
				statement.setString(1, part.toLowerCase());
				try (ResultSet result = statement.executeQuery()) {
					if (!result.next()) {
						throw new NoSuchPlayerException(part, false, true);
					}
					return getOrCreate(Utils.uuidFromString(result.getString(1)));
				}
			}
		}
	}

	public static CachedValue<PlayerContainer> getOrCreateByAnyName(String name) throws NoSuchPlayerException, SQLException {
		try {
			return getOrCreateByUsername(name);
		}
		catch (NoSuchPlayerException e) {
			// Do nothing
		}
		return getOrCreateByNickname(name);
	}

	public static CachedValue<PlayerContainer> getOrCreateByAnyNamePartial(String part) throws NoSuchPlayerException, SQLException {
		try {
			return getOrCreateByUsernamePartial(part);
		}
		catch (NoSuchPlayerException e) {
			// Do nothing
		}
		return getOrCreateByNicknamePartial(part);
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void preLogin(final AsyncPlayerPreLoginEvent event) {
		try {
			CachedValue<PlayerContainer> cached = getOrCreate(event.getUniqueId());
			cached.setCanExpire(false);
			PlayerContainer player = cached.getValue();
			if (!player.isLoaded()) {
				player.load();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			event.disallow(AsyncPlayerPreLoginEvent.Result.KICK_OTHER, "Failed to load player data\n" + e.getClass().getName() + ": " + e.getMessage());
		}
	}

	@EventHandler
	public void login(final PlayerJoinEvent event) {
		try {
			PlayerContainer container = getOrCreate(event.getPlayer().getUniqueId()).getValue();
			container.gatherData(event.getPlayer());
			container.updateLastSeen();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void logout(final PlayerQuitEvent event) {
		try {
			CachedValue<PlayerContainer> cached = getOrCreate(event.getPlayer().getUniqueId());
			cached.setCanExpire(true);
			cached.getValueSneakily().updateLastSeen();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
