package com.goldensandsmc.core.player;

import com.goldensandsmc.core.SandcastleCore;
import com.goldensandsmc.core.exception.PlayerNotOnlineException;
import com.goldensandsmc.core.player.data.Nickname;
import com.goldensandsmc.core.util.Utils;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.*;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;

/**
 * @author ShortCircuit908
 *         Created on 6/5/2017.
 */
@SuppressWarnings("unchecked")
public final class PlayerContainer {
	private final HashMap<Class<? extends PlayerDataContainer>, PlayerDataContainer> data_containers = new HashMap<>();
	private final OfflinePlayer offline_player;
	private final UUID uuid;
	private boolean loaded = false;
	private boolean exists = false;
	private String name;
	private InetAddress address;
	private Date first_joined;
	private Date last_seen;
	private boolean godmode;

	protected PlayerContainer(String name) {
		this(Utils.getOfflinePlayer(name));
	}

	protected PlayerContainer(UUID uuid) {
		this(SandcastleCore.getInstance().getServer().getOfflinePlayer(uuid));
	}

	protected PlayerContainer(OfflinePlayer offline_player) {
		this.uuid = offline_player.getUniqueId();
		this.offline_player = offline_player;
		for (Constructor<? extends PlayerDataContainer> data_constructor : PlayerManager.getDataConstructors()) {
			data_constructor.setAccessible(true);
			try {
				PlayerDataContainer container = data_constructor.newInstance(uuid);
				addDataContainer(container);
			}
			catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
				e.printStackTrace();
			}
		}
		exists = offline_player.getFirstPlayed() != 0;
	}

	public void gatherData(Player player) {
		this.name = player.getName();
		this.address = player.getAddress().getAddress();
	}

	public boolean isOnline() {
		return offline_player.isOnline();
	}

	public OfflinePlayer getOfflinePlayer() {
		return offline_player;
	}

	public Player getPlayer() throws PlayerNotOnlineException{
		if(!offline_player.isOnline()){
			throw new PlayerNotOnlineException();
		}
		return offline_player.getPlayer();
	}

	public UUID getUuid() {
		return uuid;
	}

	public String getName() {
		return name;
	}

	public InetAddress getAddress() {
		return address;
	}

	public Date getFirstJoined() {
		return first_joined;
	}

	public Date getLastSeen() {
		return last_seen;
	}

	protected void updateLastSeen() {
		last_seen = new Date();
	}

	public <T extends PlayerDataContainer> void addDataContainer(T container) {
		data_containers.put(container.getClass(), container);
	}

	public <T extends PlayerDataContainer> T getDataContainer(Class<T> container_type) {
		return (T) data_containers.get(container_type);
	}

	public String getDisplayName() {
		String nickname = getDataContainer(Nickname.class).getNickname();
		return nickname == null ? getName() : "~" + nickname;
	}

	public void save() throws SQLException {
		try (Connection connection = SandcastleCore.getInstance().getDBHandler().getConnection()) {
			// Can't be merged into a REPLACE INTO statement because it triggers deletion cascades
			try (PreparedStatement statement = connection.prepareStatement("INSERT INTO `players` (`uuid`, `name`, `address`, `first_joined`, `last_seen`) VALUES (UNHEX(?), ?, ?, ?, ?)")) {
				statement.setString(1, Utils.stringFromUuid(getUuid()));
				statement.setString(2, getName());
				statement.setBytes(3, getAddress().getAddress());
				statement.setTimestamp(4, first_joined == null ? new Timestamp(System.currentTimeMillis()) : new Timestamp(first_joined.getTime()));
				statement.setTimestamp(5, last_seen == null ? new Timestamp(System.currentTimeMillis()) : new Timestamp(last_seen.getTime()));
				statement.execute();
			}
			catch (SQLException e) {
				try (PreparedStatement statement = connection.prepareStatement("UPDATE `players` SET `name`=?, `address`=?, `first_joined`=?, `last_seen`=? WHERE `uuid`=UNHEX(?)")) {
					statement.setString(1, getName());
					statement.setBytes(2, getAddress().getAddress());
					statement.setTimestamp(3, first_joined == null ? new Timestamp(System.currentTimeMillis()) : new Timestamp(first_joined.getTime()));
					statement.setTimestamp(4, last_seen == null ? new Timestamp(System.currentTimeMillis()) : new Timestamp(last_seen.getTime()));
					statement.setString(5, Utils.stringFromUuid(getUuid()));
					statement.execute();
				}
			}
			for (PlayerDataContainer container : data_containers.values()) {
				try {
					container.save(connection);
				}
				catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void load() throws SQLException {
		try (Connection connection = SandcastleCore.getInstance().getDBHandler().getConnection()) {
			try (PreparedStatement statement = connection.prepareStatement("SELECT `name`, `address`, `first_joined`, `last_seen` FROM `players` WHERE `uuid`=UNHEX(?)")) {
				statement.setString(1, Utils.stringFromUuid(getUuid()));
				try (ResultSet result = statement.executeQuery()) {
					if (result.next()) {
						exists = true;
						name = result.getString(1);
						byte[] raw_address = result.getBytes(2);
						first_joined = result.getTimestamp(3);
						last_seen = result.getTimestamp(4);
						try {
							address = raw_address == null ? null : InetAddress.getByAddress(raw_address);
						}
						catch (UnknownHostException e) {
							e.printStackTrace();
						}
						loaded = true;
					}
					else {
						exists = false;
					}
				}
			}
			for (PlayerDataContainer container : data_containers.values()) {
				try {
					container.load(connection);
				}
				catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public boolean isLoaded() {
		return loaded;
	}

	public boolean exists() {
		return exists;
	}

	public boolean isGodmode(){
		return godmode;
	}

	public void setGodmode(boolean godmode){
		this.godmode = godmode;
	}
}
