package com.goldensandsmc.core.player;

import com.goldensandsmc.core.SandcastleCore;
import org.bukkit.entity.Player;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;
import java.util.UUID;

/**
 * @author ShortCircuit908
 *         Created on 4/17/2017.
 */
public abstract class PlayerDataContainer {
	private final UUID player_uuid;

	public PlayerDataContainer(UUID player_uuid) {
		this.player_uuid = player_uuid;
	}

	public UUID getPlayerUuid() {
		return player_uuid;
	}

	public final void save() throws SQLException {
		try (Connection connection = SandcastleCore.getInstance().getDBHandler().getConnection()) {
			save(connection);
		}
	}

	public final void load() throws SQLException {
		try (Connection connection = SandcastleCore.getInstance().getDBHandler().getConnection()) {
			load(connection);
		}
	}

	public abstract void load(Connection connection) throws SQLException;

	public abstract void save(Connection connection) throws SQLException;

	public abstract void apply(Player player);

	public abstract Map<String, Object> getMapping();
}
