package com.goldensandsmc.core.commands;

import com.goldensandsmc.core.exception.CooldownException;
import com.goldensandsmc.core.util.Utils;
import com.shortcircuit.utils.bukkit.command.exceptions.CommandException;
import com.shortcircuit.utils.collect.ConcurrentArrayList;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.util.HashMap;
import java.util.UUID;

/**
 * @author ShortCircuit908
 *         Created on 6/10/2017.
 */
public abstract class CooldownCommand<T extends CommandSender> extends CoreCommand<T> {
	private final HashMap<UUID, Long> last_used = new HashMap<>();

	public CooldownCommand(Plugin plugin) {
		super(plugin);
	}

	public abstract int getCooldownSeconds();

	public Integer getCooldownSeconds(CommandSender sender) {
		return Utils.getNumericPermission(sender, getCommandPermission() + ".cooldown");
	}

	public boolean canUseAgain(CommandSender sender) {
		if (!(sender instanceof Player) || sender.isOp() || sender.hasPermission(getCommandPermission() + ".cooldown.bypass")) {
			return true;
		}
		Player player = (Player) sender;
		if (!last_used.containsKey(player.getUniqueId())) {
			return true;
		}
		long now = System.currentTimeMillis();
		Integer seconds = getCooldownSeconds(sender);
		if (seconds == null) {
			seconds = getCooldownSeconds();
		}
		long cooldown = seconds * 1000;

		long last_used_time = last_used.get(player.getUniqueId());
		return now - last_used_time >= cooldown;
	}

	@Override
	public String[] exec(T sender, String command, ConcurrentArrayList<String> args) throws CommandException {
		if (!canUseAgain(sender) && sender instanceof Player) {
			int elapsed = (int) (getCooldownSeconds() * 1000 - (System.currentTimeMillis() - last_used.get(((Player) sender).getUniqueId())));
			throw new CooldownException(elapsed);
		}
		String[] result = super.exec(sender, command, args);
		if (sender instanceof Player) {
			last_used.put(((Player) sender).getUniqueId(), System.currentTimeMillis());
		}
		return result;
	}
}
