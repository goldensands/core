package com.goldensandsmc.core.commands;

import com.shortcircuit.utils.bukkit.command.BaseCommand;
import com.shortcircuit.utils.bukkit.command.exceptions.CommandException;
import com.shortcircuit.utils.collect.ConcurrentArrayList;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.Plugin;

import java.sql.SQLException;

/**
 * @author ShortCircuit908
 *         Created on 6/6/2017.
 */
public abstract class CoreCommand<T extends CommandSender> extends BaseCommand<T> {
	public CoreCommand(Plugin plugin) {
		super(plugin);
	}

	public abstract String[] execute(T sender, String command, ConcurrentArrayList<String> args) throws CommandException, SQLException;

	@Override
	public String[] exec(T sender, String command, ConcurrentArrayList<String> args) throws CommandException {
		try {
			return execute(sender, command, args);
		}
		catch (SQLException e) {
			e.printStackTrace();
			throw new CommandException("There was an error with the database. Please contact an administrator.");
		}
	}
}
