package com.goldensandsmc.core.commands;

import com.goldensandsmc.core.exception.ContentTooLongException;
import com.goldensandsmc.core.player.PlayerContainer;
import com.goldensandsmc.core.player.PlayerManager;
import com.goldensandsmc.core.player.data.Nickname;
import com.shortcircuit.utils.bukkit.command.CommandType;
import com.shortcircuit.utils.bukkit.command.exceptions.*;
import com.shortcircuit.utils.collect.ConcurrentArrayList;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.sql.SQLException;

/**
 * @author ShortCircuit908
 *         Created on 6/5/2017.
 */
public class NicknameCommand extends CoreCommand<CommandSender> {

	public NicknameCommand(Plugin plugin) {
		super(plugin);
	}

	@Override
	public String getCommandName() {
		return "nick";
	}

	@Override
	public String[] getCommandAliases() {
		return new String[]{"nickname"};
	}

	@Override
	public CommandType getCommandType() {
		return CommandType.ANY;
	}

	@Override
	public String[] getCommandDescription() {
		return new String[]{"Change your nickname"};
	}

	@Override
	public String[] getCommandUsage() {
		return new String[]{"/<command> [player] <nickname|off>"};
	}

	@Override
	public String getCommandPermission() {
		return "sandcastle.core.command.nick";
	}

	@Override
	public String[] execute(CommandSender sender, String command, ConcurrentArrayList<String> args) throws CommandException, SQLException {
		PlayerContainer target;
		if (args.size() == 0) {
			throw new TooFewArgumentsException();
		}
		if (args.size() > 2) {
			throw new TooManyArgumentsException();
		}
		if (args.size() == 2) {
			if (!sender.hasPermission(getCommandPermission() + ".others")) {
				throw new NoPermissionException(getCommandPermission() + ".others");
			}
			target = PlayerManager.getOrCreateByAnyName(args.get(0)).getValue();
		}
		else {
			if (!(sender instanceof Player)) {
				throw new PlayerOnlyException();
			}
			target = PlayerManager.getOrCreate(((Player) sender).getUniqueId()).getValue();
		}
		String raw_nickname = args.get(args.size() - 1);
		if (raw_nickname.equalsIgnoreCase("off")) {
			raw_nickname = null;
		}
		else {
			if (!raw_nickname.matches("[a-zA-Z0-9_&]+")) {
				throw new InvalidArgumentException("Nickname may not contain special characters");
			}
			if (raw_nickname.indexOf('&') > -1 && !sender.hasPermission(getCommandPermission() + ".format")) {
				throw new InvalidArgumentException("You may not use format codes in your nickname");
			}
			raw_nickname = ChatColor.translateAlternateColorCodes('&', raw_nickname).replace("&", "");
			if (raw_nickname.length() > 16) {
				throw new ContentTooLongException(16);
			}
			Object[] result = Nickname.nicknameInUse(raw_nickname);
			if ((boolean)result[0] && !target.getUuid().equals(result[1])) {
				throw new InvalidArgumentException("That nickname is in use");
			}
		}
		Nickname nickname = target.getDataContainer(Nickname.class);
		if (!nickname.canChange() && !sender.hasPermission(getCommandPermission() + ".override")) {
			throw new CommandException("You must wait to change your nickname again");
		}
		nickname.setNickname(raw_nickname);
		if (target.isOnline()) {
			nickname.apply(target.getPlayer());
		}
		boolean self = target.getName().equals(sender.getName());
		if (raw_nickname == null) {
			return new String[]{
					"Removed " + ChatColor.GOLD + (self ? "your" : target.getName() + "'s") + ChatColor.AQUA + " nickname"
			};
		}
		return new String[]{
				"Set " + ChatColor.GOLD + (self ? "your" : target.getName() + "'s") + ChatColor.AQUA + " nickname to "
						+ ChatColor.GOLD + target.getDisplayName()
		};
	}
}
