package com.goldensandsmc.core.commands;

import com.goldensandsmc.core.player.PlayerContainer;
import com.goldensandsmc.core.player.PlayerManager;
import com.shortcircuit.utils.bukkit.command.CommandType;
import com.shortcircuit.utils.bukkit.command.exceptions.CommandException;
import com.shortcircuit.utils.bukkit.command.exceptions.TooFewArgumentsException;
import com.shortcircuit.utils.collect.ConcurrentArrayList;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.Plugin;

import java.sql.SQLException;

/**
 * @author ShortCircuit908
 *         Created on 6/7/2017.
 */
public class RealnameCommand extends CoreCommand<CommandSender> {

	public RealnameCommand(Plugin plugin) {
		super(plugin);
	}

	@Override
	public String getCommandName() {
		return "realname";
	}

	@Override
	public String[] getCommandAliases() {
		return new String[]{"rn"};
	}

	@Override
	public CommandType getCommandType() {
		return CommandType.ANY;
	}

	@Override
	public String[] getCommandDescription() {
		return new String[]{"Shows a player's real name"};
	}

	@Override
	public String[] getCommandUsage() {
		return new String[]{"/<command> <player>"};
	}

	@Override
	public String getCommandPermission() {
		return "sandcastle.core.command.realname";
	}

	@Override
	public String[] execute(CommandSender sender, String command, ConcurrentArrayList<String> args) throws CommandException, SQLException {
		if (args.size() < 1) {
			throw new TooFewArgumentsException();
		}
		PlayerContainer target = PlayerManager.getOrCreateByNickname(args.get(0)).getValue();
		return new String[]{target.getDisplayName() + "'s real name is " + ChatColor.GOLD + target.getName()};
	}
}
