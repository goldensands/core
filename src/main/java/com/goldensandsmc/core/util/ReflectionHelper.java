package com.goldensandsmc.core.util;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import org.bukkit.Bukkit;
import org.bukkit.Server;

/**
 * @author ShortCircuit908
 *         Created on 6/26/2017.
 */
public class ReflectionHelper {
	public static final Package CRAFTBUKKIT_PACKAGE;
	public static final Package NMS_PACKAGE;

	static {
		Class<? extends Server> cb_server_class = Bukkit.getServer().getClass();
		Class<?> nms_server_class;
		Package temp = null;
		try {
			Method method_handle = cb_server_class.getDeclaredMethod("getServer");
			method_handle.setAccessible(true);
			nms_server_class = method_handle.invoke(Bukkit.getServer()).getClass();
			temp = nms_server_class.getPackage();
		}
		catch (ReflectiveOperationException e) {
			e.printStackTrace();
		}
		NMS_PACKAGE = temp;
		CRAFTBUKKIT_PACKAGE = cb_server_class.getPackage();
	}

	public static Class<?> getCraftbukkitClass(String name) throws ClassNotFoundException {
		return Class.forName(CRAFTBUKKIT_PACKAGE.getName() + "." + name);
	}

	public static Class<?> getNMSClass(String name) throws ClassNotFoundException {
		return Class.forName(NMS_PACKAGE.getName() + "." + name);
	}

	public static Field getField(Class<?> clazz, String name) throws NoSuchFieldException {
		try {
			return clazz.getDeclaredField(name);
		}
		catch (NoSuchFieldException e) {
			if (clazz.getSuperclass() != null) {
				return getField(clazz.getSuperclass(), name);
			}
		}
		throw new NoSuchFieldException(name);
	}

	public static Method getMethod(Class<?> clazz, String name, Class<?>... parameter_types) throws NoSuchMethodException {
		try {
			return clazz.getDeclaredMethod(name, parameter_types);
		}
		catch (NoSuchMethodException e) {
			if (clazz.getSuperclass() != null) {
				return getMethod(clazz.getSuperclass(), name, parameter_types);
			}
		}
		throw new NoSuchMethodException(name);
	}

	public static <T> Constructor<T> getConstructor(Class<T> clazz, Class<?>... parameter_types) throws NoSuchMethodException {
		return clazz.getDeclaredConstructor(parameter_types);
	}

	public static Field[] getAllFields(Class<?> clazz) {
		LinkedList<Field> fields = new LinkedList<>();
		getAllFields(clazz, fields);
		return fields.toArray(new Field[0]);
	}

	private static void getAllFields(Class<?> clazz, List<Field> fields) {
		Collections.addAll(fields, clazz.getDeclaredFields());
		if (clazz.getSuperclass() != null) {
			getAllFields(clazz.getSuperclass(), fields);
		}
	}

	public static Method[] getAllMethods(Class<?> clazz) {
		LinkedList<Method> fields = new LinkedList<>();
		getAllMethods(clazz, fields);
		return fields.toArray(new Method[0]);
	}

	private static void getAllMethods(Class<?> clazz, List<Method> methods) {
		Collections.addAll(methods, clazz.getDeclaredMethods());
		if (clazz.getSuperclass() != null) {
			getAllMethods(clazz.getSuperclass(), methods);
		}
	}
}
