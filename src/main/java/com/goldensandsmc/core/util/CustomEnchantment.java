package com.goldensandsmc.core.util;

import org.bukkit.enchantments.Enchantment;
import org.bukkit.enchantments.EnchantmentTarget;
import org.bukkit.inventory.ItemStack;

/**
 * @author ShortCircuit908
 *         Created on 6/8/2017.
 */
public class CustomEnchantment extends Enchantment {
	private final String name;
	private final int max_level;
	private final int start_level;
	private final boolean treasure;
	private final boolean cursed;
	private final EnchantmentTarget item_target;

	public CustomEnchantment(int id, String name, int max_level, int start_level, EnchantmentTarget item_target){
		this(id, name, max_level, start_level, false, false, item_target);
	}

	public CustomEnchantment(int id, String name, int max_level, int start_level, boolean treasure, boolean cursed, EnchantmentTarget item_target) {
		super(id);
		this.name = name.toUpperCase();
		this.max_level = max_level;
		this.start_level = start_level;
		this.treasure = treasure;
		this.cursed = cursed;
		this.item_target = item_target;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public int getMaxLevel() {
		return max_level;
	}

	@Override
	public int getStartLevel() {
		return start_level;
	}

	public boolean isTreasure() {
		return treasure;
	}

	public boolean isCursed() {
		return cursed;
	}

	@Override
	public EnchantmentTarget getItemTarget() {
		return item_target;
	}

	@Override
	public boolean conflictsWith(Enchantment other) {
		return false;
	}

	@Override
	public boolean canEnchantItem(ItemStack item) {
		return getItemTarget().includes(item);
	}

	public boolean register() throws IllegalArgumentException {
		return EnchantmentHelper.registerCustomEnchantment(this);
	}
}
