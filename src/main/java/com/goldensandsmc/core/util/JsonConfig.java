package com.goldensandsmc.core.util;

import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import org.apache.commons.lang3.reflect.TypeUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.HashMap;
import java.util.Scanner;

/**
 * @author ShortCircuit908
 *         Created on 6/6/2017.
 */
public class JsonConfig {
	private final ClassLoader context;
	private final File file;
	private final String default_path;
	private JsonObject root;
	private final HashMap<String, Object> value_cache = new HashMap<>();

	public JsonConfig(File file) {
		this(null, file, null);
	}

	public JsonConfig(Object context, File file, String default_path) {
		this.file = file;
		file.getParentFile().mkdirs();
		this.default_path = default_path;
		this.context = context == null ? null : context.getClass().getClassLoader();
		saveDefaultConfig();
		loadConfig();
	}

	public synchronized void loadConfig() {
		synchronized (this) {
			try {
				if (!file.exists()) {
					throw new IllegalStateException("Configuration file not found");
				}
				StringBuilder builder = new StringBuilder();
				Scanner scanner = new Scanner(file);
				while (scanner.hasNextLine()) {
					String line = scanner.nextLine();
					builder.append(line).append('\n');
				}
				scanner.close();
				String data = builder.toString().replaceAll("(/\\*(.*)\\*/)|(//(.*))", "");
				value_cache.clear();
				root = JsonUtils.getGson().fromJson(data, JsonObject.class);
				if (root == null) {
					root = new JsonObject();
				}
			}
			catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}
	}

	public synchronized void saveConfig() {
		synchronized (this) {
			try {
				file.createNewFile();
				FileWriter writer = new FileWriter(file);
				writer.write(JsonUtils.getGson().toJson(root));
				writer.flush();
				writer.close();
			}
			catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public synchronized void saveDefaultConfig() {
		synchronized (this) {
			saveDefaultConfig(false);
		}
	}

	public synchronized void saveDefaultConfig(boolean overwrite) {
		synchronized (this) {
			if (file.exists() && !overwrite) {
				return;
			}
			try {
				file.createNewFile();
				if (context != null && default_path != null) {
					Files.copy(context.getResourceAsStream(default_path), file.toPath(), StandardCopyOption.REPLACE_EXISTING);
				}
				else {
					saveConfig();
				}
			}
			catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public synchronized <T> void setNode(String path, T value) {
		synchronized (this) {
			JsonObject cur_path = root;
			String[] nodes = path.split("\\.");
			for (int i = 0; i < nodes.length - 1; i++) {
				if (!cur_path.has(nodes[i])) {
					cur_path.add(nodes[i], new JsonObject());
				}
				cur_path = cur_path.getAsJsonObject(nodes[i]);
			}
			cur_path.add(nodes[nodes.length - 1], JsonUtils.getGson().toJsonTree(value));
			value_cache.put(path, value);
		}
	}

	public <T> T getNode(String path, Type type, T default_if_null) {
		synchronized (this) {
			if (value_cache.containsKey(path)) {
				Object value = value_cache.get(path);
				if (value == null || TypeUtils.isAssignable(value.getClass(), type)) {
					return (T) value;
				}
			}
			JsonObject cur_path = root;
			String[] nodes = path.split("\\.");
			for (int i = 0; i < nodes.length - 1; i++) {
				if (!cur_path.has(nodes[i])) {
					if (default_if_null == null) {
						value_cache.put(path, null);
						return null;
					}
					cur_path.add(nodes[i], new JsonObject());
				}
				cur_path = cur_path.getAsJsonObject(nodes[i]);
			}
			if (!cur_path.has(nodes[nodes.length - 1]) && default_if_null != null) {
				cur_path.add(nodes[nodes.length - 1], JsonUtils.getGson().toJsonTree(default_if_null, type));
			}
			JsonElement raw_value = cur_path.get(nodes[nodes.length - 1]);
			T value = raw_value == null || raw_value instanceof JsonNull ? default_if_null : JsonUtils.getGson().fromJson(raw_value, type);
			value_cache.put(path, value);
			return value;
		}
	}

	public <T> T getNode(String node, Class<T> type, T default_if_null) {
		return getNode(node, (Type) type, default_if_null);
	}
}
