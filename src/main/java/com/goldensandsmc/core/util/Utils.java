package com.goldensandsmc.core.util;

import blogspot.software_and_algorithms.stern_library.string.DamerauLevenshteinAlgorithm;
import com.goldensandsmc.core.SandcastleCore;
import com.goldensandsmc.core.exception.ContentTooLongException;
import com.google.common.collect.ImmutableMap;
import com.google.common.reflect.TypeToken;
import com.opencsv.CSVReader;
import com.shortcircuit.utils.bukkit.command.exceptions.InvalidArgumentException;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang.WordUtils;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.DyeColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.permissions.Permissible;
import org.bukkit.permissions.PermissionAttachmentInfo;

/**
 * @author ShortCircuit908
 *         Created on 6/5/2017.
 */
public class Utils {
	private static final File items_file = new File(SandcastleCore.getInstance().getDataFolder() + File.separator + "items.csv");
	private static final NumberFormat short_format = new DecimalFormat("0.#");
	public static final Random RANDOM = new Random();
	private static final Set<Material> transparent_materials = new HashSet<>();
	private static final Set<Material> nonsolid_materials = new HashSet<>();
	private static final Map<String, Color> color_lookup = ImmutableMap.<String, Color>builder()
			.put("WHITE", Color.WHITE)
			.put("WHITE2", DyeColor.WHITE.getColor())
			.put("WHITE3", DyeColor.WHITE.getFireworkColor())
			.put("SILVER", Color.SILVER)
			.put("SILVER2", DyeColor.SILVER.getColor())
			.put("SILVER3", DyeColor.SILVER.getFireworkColor())
			.put("GRAY", Color.GRAY)
			.put("GREY", Color.GRAY)
			.put("GRAY2", DyeColor.GRAY.getColor())
			.put("GREY2", DyeColor.GRAY.getColor())
			.put("GRAY3", DyeColor.GRAY.getFireworkColor())
			.put("GREY3", DyeColor.GRAY.getFireworkColor())
			.put("BLACK", Color.BLACK)
			.put("BLACK2", DyeColor.BLACK.getColor())
			.put("BLACK3", DyeColor.BLACK.getFireworkColor())
			.put("RED", Color.RED)
			.put("RED2", DyeColor.RED.getColor())
			.put("RED3", DyeColor.RED.getFireworkColor())
			.put("MAROON", Color.MAROON)
			.put("YELLOW", Color.YELLOW)
			.put("YELLOW2", DyeColor.YELLOW.getColor())
			.put("YELLOW3", DyeColor.YELLOW.getFireworkColor())
			.put("OLIVE", Color.OLIVE)
			.put("LIME", Color.LIME)
			.put("LIME2", DyeColor.LIME.getColor())
			.put("LIME3", DyeColor.LIME.getFireworkColor())
			.put("GREEN", Color.GREEN)
			.put("GREEN2", DyeColor.GREEN.getColor())
			.put("GREEN3", DyeColor.GREEN.getFireworkColor())
			.put("AQUA", Color.AQUA)
			.put("TEAL", Color.TEAL)
			.put("BLUE", Color.BLUE)
			.put("BLUE2", DyeColor.BLUE.getColor())
			.put("BLUE3", DyeColor.BLUE.getFireworkColor())
			.put("NAVY", Color.NAVY)
			.put("FUCHSIA", Color.FUCHSIA)
			.put("PURPLE", Color.PURPLE)
			.put("PURPLE2", DyeColor.PURPLE.getColor())
			.put("PURPLE3", DyeColor.PURPLE.getFireworkColor())
			.put("ORANGE", Color.ORANGE)
			.put("ORANGE2", DyeColor.ORANGE.getColor())
			.put("ORANGE3", DyeColor.ORANGE.getFireworkColor())
			.put("PINK", DyeColor.PINK.getColor())
			.put("PINK2", DyeColor.PINK.getFireworkColor())
			.put("CYAN", DyeColor.CYAN.getColor())
			.put("CYAN2", DyeColor.CYAN.getFireworkColor())
			.put("BROWN", DyeColor.BROWN.getColor())
			.put("BROWN2", DyeColor.BROWN.getFireworkColor())
			.put("LIGHT_BLUE", DyeColor.LIGHT_BLUE.getColor())
			.put("LIGHT_BLUE2", DyeColor.LIGHT_BLUE.getFireworkColor())
			.put("MAGENTA", DyeColor.MAGENTA.getColor())
			.put("MAGENTA2", DyeColor.MAGENTA.getFireworkColor())
			.build();

	static {
		for (Material material : Material.values()) {
			if (material.isTransparent()) {
				transparent_materials.add(material);
			}
			if (material.isBlock() && !material.isSolid()) {
				nonsolid_materials.add(material);
			}
		}
		if (!items_file.exists()) {
			try {
				items_file.createNewFile();
				//Files.copy(SandcastleCore.class.getResourceAsStream("items.csv"), items_file.toPath(), StandardCopyOption.REPLACE_EXISTING);
			}
			catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static boolean isSimilar(String a, String b) {
		a = a.toLowerCase();
		b = b.toLowerCase();
		if (a.equalsIgnoreCase(b)) {
			return true;
		}
		if (!SandcastleCore.getInstance().getJsonConfig().getNode("nicknames.disallow_similar", boolean.class, true)) {
			return false;
		}
		DamerauLevenshteinAlgorithm algorithm = new DamerauLevenshteinAlgorithm(1, 1, 1, 1);
		return algorithm.execute(a, b) < 3;
	}

	public static String stringFromUuid(UUID uuid) {
		return uuid == null ? null : uuid.toString().replace("-", "");
	}

	public static UUID uuidFromString(String string) throws IllegalArgumentException {
		return string == null ? null : UUID.fromString(string.replaceAll("([0-9a-fA-F]{8})-?([0-9a-fA-F]{4})-?([0-9a-fA-F]{4})-?([0-9a-fA-F]{4})-?([0-9a-fA-F]{12})", "$1-$2-$3-$4-$5"));
	}

	public static OfflinePlayer getOfflinePlayer(String name) {
		OfflinePlayer offline_player = SandcastleCore.getInstance().getServer().getOfflinePlayer(name);
		if (offline_player.getFirstPlayed() != 0) {
			return offline_player;
		}
		for (OfflinePlayer test_player : SandcastleCore.getInstance().getServer().getOfflinePlayers()) {
			if (test_player.getName().equalsIgnoreCase(offline_player.getName())) {
				offline_player = test_player;
				break;
			}
		}
		return offline_player;
	}

	public static int getLastInsertId(Connection connection) throws SQLException {
		try (PreparedStatement statement = connection.prepareStatement("SELECT LAST_INSERT_ID()")) {
			try (ResultSet result = statement.executeQuery()) {
				if (result.next()) {
					return result.getInt(1);
				}
				return -1;
			}
		}
	}

	public static void validateInput(String text, boolean allow_speical_chars) throws InvalidArgumentException {
		validateInput(text, allow_speical_chars, Integer.MAX_VALUE);
	}

	public static void validateInput(String text, boolean allow_speical_chars, int max_length) throws InvalidArgumentException {
		if (text.length() > max_length) {
			throw new ContentTooLongException(max_length);
		}
		if (!allow_speical_chars && !text.matches("[a-zA-Z0-1_]+")) {
			throw new InvalidArgumentException("Argument cannot contain special characters");
		}
	}

	public static void teleportWithLeashed(Entity target, Entity destination, PlayerTeleportEvent.TeleportCause cause) {
		teleportWithLeashed(target, destination.getLocation(), cause);
	}

	public static void teleportWithLeashed(Entity target, Location destination, PlayerTeleportEvent.TeleportCause cause) {
		LinkedList<Entity> affected = new LinkedList<>();
		affected.add(target);
		for (LivingEntity entity : target.getWorld().getLivingEntities()) {
			if (entity.isLeashed() && entity.getLeashHolder().equals(target)) {
				affected.add(entity);
			}
		}
		for (Entity entity : affected) {
			entity.teleport(destination, cause);
		}
	}

	public static void teleportWithLeashed(Entity target, Entity destination) {
		teleportWithLeashed(target, destination.getLocation());
	}

	public static void teleportWithLeashed(Entity target, Location destination) {
		LinkedList<Entity> affected = new LinkedList<>();
		affected.add(target);
		for (LivingEntity entity : target.getWorld().getLivingEntities()) {
			if (entity.isLeashed() && entity.getLeashHolder().equals(target)) {
				affected.add(entity);
			}
		}
		for (Entity entity : affected) {
			entity.teleport(destination);
		}
	}

	public static Integer getNumericPermission(Permissible permissible, String permission) {
		//permissible.getEffectivePermissions().add(new PermissionAttachmentInfo(permissible, permission + ".5", new PermissionAttachment(SandcastleCore.getInstance(), permissible), true));
		permissible.addAttachment(SandcastleCore.getInstance(), permission + ".5", true);
		for (PermissionAttachmentInfo attachment_info : permissible.getEffectivePermissions()) {
			String stripped = attachment_info.getPermission().replaceAll("^" + permission.replace(".", "\\.") + "\\.", "");
			try {
				return Integer.parseInt(stripped);
			}
			catch (NumberFormatException e) {
				// Do nothing
			}
		}
		return null;
	}

	public static Block getTargetBlock(Player player, int max_distance, boolean ignore_transparent, boolean ignore_nonsolid) {
		Set<Material> ignore = new HashSet<>();
		if (ignore_transparent) {
			ignore.addAll(transparent_materials);
		}
		if (ignore_nonsolid) {
			ignore.addAll(nonsolid_materials);
		}
		return getTargetBlock(player, max_distance, ignore);
	}

	public static Block getTargetBlock(Player player, int max_distance, Set<Material> ignore) {
		try {
			Method method = player.getClass().getMethod("getTargetBlock", new TypeToken<Set<Material>>() {
			}.getRawType(), int.class);
			return (Block) method.invoke(player, ignore, max_distance);
		}
		catch (ReflectiveOperationException e) {
			// Do nothing
		}
		HashSet<Byte> dumb_stupid_thing = new HashSet<>();
		for (Material material : ignore) {
			dumb_stupid_thing.add((byte) material.getId());
		}
		return player.getTargetBlock(dumb_stupid_thing, max_distance);
	}

	public static String getFriendlyName(String raw_name) {
		return WordUtils.capitalizeFully(raw_name.replace('_', ' '));
	}

	public static String formatTime(long millis, ChatColor default_color, ChatColor value_color) {
		System.out.println("millis = " + millis);
		double seconds = millis / 1000.0;
		millis -= seconds * 1000;
		long minutes = (long) (seconds / 60);
		seconds -= minutes * 60;
		long hours = minutes / 60;
		minutes -= hours * 60;
		long days = hours / 24;
		hours -= days * 24;
		long years = Math.round(days / 365.25);
		days -= Math.round(years * 365.25);

		StringBuilder builder = new StringBuilder();
		if (years > 0) {
			builder.append(value_color).append(years).append('y').append(default_color).append(", ");
		}
		if (days > 0) {
			builder.append(value_color).append(days).append('d').append(default_color).append(", ");
		}
		if (hours > 0) {
			builder.append(value_color).append(hours).append('h').append(default_color).append(", ");
		}
		if (minutes > 0) {
			builder.append(value_color).append(minutes).append('m').append(default_color).append(", ");
		}
		if (seconds > 0 || millis > 0) {
			builder.append(value_color).append(short_format.format(seconds)).append('s').append(default_color).append(", ");
		}
		return builder.delete(builder.length() - 2, builder.length()).toString();
	}

	public static String getFriendlyClassName(Class<?> clazz) {
		if (clazz.isArray()) {
			return "List[" + getFriendlyClassName(clazz.getComponentType()) + "]";
		}
		if (Collection.class.isAssignableFrom(clazz)) {
			return "List";
		}
		if (Number.class.isAssignableFrom(clazz)) {
			return "Number";
		}
		if (CharSequence.class.isAssignableFrom(clazz)) {
			return "Text";
		}
		return clazz.getSimpleName();
	}

	public static ItemStack getItemFromString(String raw_type) throws InvalidArgumentException {
		raw_type = raw_type.toLowerCase().replace("_", "");
		Material type = null;
		Integer id = null;
		Short durability = null;
		if (raw_type.indexOf(':') > -1) {
			String raw_durability = raw_type.substring(raw_type.indexOf(':') + 1);
			raw_type = raw_type.substring(0, raw_type.indexOf(':'));
			if (!raw_durability.trim().isEmpty()) {
				try {
					durability = Short.parseShort(raw_durability);
				}
				catch (NumberFormatException e) {
					throw new InvalidArgumentException(e);
				}
			}
		}
		try {
			id = Integer.parseInt(raw_type);
		}
		catch (NumberFormatException e) {
			// Do nothing
		}
		if (id == null) {
			for (Material material : Material.values()) {
				if (material.name().replace("_", "").toLowerCase().equals(raw_type)) {
					type = material;
					break;
				}
			}
		}
		if (type == null && id == null) {
			try (CSVReader reader = new CSVReader(new FileReader(items_file))) {
				String[] line;
				while ((line = reader.readNext()) != null) {
					if (line.length < 2) {
						continue;
					}
					if (line[0].equals(raw_type)) {
						id = Integer.parseInt(line[1]);
						if (durability == null && line.length > 2) {
							durability = Short.parseShort(line[2]);
						}
						break;
					}
				}
			}
			catch (IOException e) {
				e.printStackTrace();
			}
		}
		if (type == null && id != null) {
			for (Material material : Material.values()) {
				if (material.getId() == id) {
					type = material;
					break;
				}
			}
		}
		if (durability == null) {
			durability = 0;
		}
		if (type == null) {
			throw new InvalidArgumentException("Unknown type \"" + raw_type + "\"");
		}
		return new ItemStack(type, 1, durability);
	}

	public static int compareVersions(int[] v1, int[] v2) {
		for (int i = 0; i < Math.min(v1.length, v2.length); i++) {
			if (v1[i] > v2[i]) {
				return 1;
			}
			if (v1[i] < v2[i]) {
				return -1;
			}
		}
		if (v1.length > v2.length) {
			return 1;
		}
		if (v1.length < v2.length) {
			return -1;
		}
		return 0;
	}

	public static int[] stringToVersion(String version) {
		Pattern pattern = Pattern.compile("(\\d+\\.?)*");
		Matcher matcher = pattern.matcher(version);
		if (matcher.find()) {
			String find = matcher.group(0);
			String[] parts = find.split("\\.");
			int[] parsed = new int[parts.length];
			for (int i = 0; i < parts.length; i++) {
				parsed[i] = Integer.parseInt(parts[i]);
			}
			int cut_index;
			for (cut_index = parsed.length; cut_index >= 0; cut_index--) {
				if (parsed[cut_index - 1] != 0) {
					break;
				}
			}
			if (cut_index != parsed.length) {
				int[] new_parsed = new int[cut_index];
				System.arraycopy(parsed, 0, new_parsed, 0, cut_index);
				parsed = new_parsed;
			}
			System.out.println("parsed = " + Arrays.toString(parsed));
			return parsed;
		}
		return new int[0];
	}

	public static String stringFromGameTime(long game_time) {
		return DateFormat.getTimeInstance(DateFormat.SHORT).format(new Date(game_time * 3600));
	}

	public static int gameTimeFromString(String time_string) throws NumberFormatException, InvalidArgumentException {
		time_string = time_string.trim().toLowerCase();
		int ticks;
		if (time_string.matches("\\d{1,2}(:\\d{2}){0,2}?(am|pm)?")) {
			String[] parts = time_string.split(":");
			int hours = Integer.parseInt(parts[0]);
			if (parts[parts.length - 1].matches("\\d{2}(am|pm)")) {
				if (parts[parts.length - 1].endsWith("pm")) {
					hours += 12;
				}
				parts[parts.length - 1] = parts[parts.length - 1].replaceAll("(\\d{2})(am|pm)", "$1");
			}
			int minutes = parts.length > 1 ? Integer.parseInt(parts[1]) : 0;
			int seconds = parts.length > 2 ? Integer.parseInt(parts[2]) : 0;
			minutes += hours * 60;
			seconds += minutes * 60;
			ticks = (int) ((Math.round(seconds / 3.6) + 18000) % 24000);
		}
		else if (time_string.matches("\\d+(t(ick(s)?)?)?")) {
			time_string = time_string.replaceAll("(\\d+)(t(ick(s)?)?)?", "$1");
			ticks = Integer.parseInt(time_string) % 24000;
		}
		else if (time_string.equals("dawn")) {
			ticks = 0;
		}
		else if (time_string.equals("noon") || time_string.equals("day")) {
			ticks = 6000;
		}
		else if (time_string.equals("dusk")) {
			ticks = 12000;
		}
		else if (time_string.equals("midnight") || time_string.equals("night")) {
			ticks = 18000;
		}
		else {
			throw new InvalidArgumentException("Cannot parse time string: " + time_string);
		}
		return ticks;
	}

	public static String formatTrueFalse(boolean value) {
		return (value ? ChatColor.GREEN : ChatColor.RED) + String.valueOf(value);
	}

	public static String formatYesNo(boolean value) {
		return value ? ChatColor.GREEN + "yes" : ChatColor.RED + "no";
	}

	public static Color getColorFromName(String name) {
		return color_lookup.get(name.toUpperCase());
	}

	public static String getNameFromColor(Color color) {
		for (Map.Entry<String, Color> entry : color_lookup.entrySet()) {
			if (entry.getValue().equals(color)) {
				return entry.getKey();
			}
		}
		return null;
	}
}
