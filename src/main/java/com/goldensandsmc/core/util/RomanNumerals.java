package com.goldensandsmc.core.util;

/**
 * @author ShortCircuit908
 *         Created on 6/28/2017.
 */
public class RomanNumerals {
	private static final Object[][] roman_to_num = {
			{900, "CM"},
			{1000, "M"},
			{400, "CD"},
			{500, "D"},
			{90, "XC"},
			{100, "C"},
			{40, "XL"},
			{50, "L"},
			{9, "IX"},
			{10, "X"},
			{4, "IV"},
			{5, "V"},
			{1, "I"}
	};
	private static final Object[][] num_to_roman = {
			{1000, "M"},
			{900, "CM"},
			{500, "D"},
			{400, "CD"},
			{100, "C"},
			{90, "XC"},
			{50, "L"},
			{40, "XL"},
			{10, "X"},
			{9, "IX"},
			{5, "V"},
			{4, "IV"},
			{1, "I"}
	};

	public static String numberToRomanNumerals(long value) {
		if (value == 0) {
			return "";
		}
		String sign = value < 0 ? "-" : "";
		value = Math.abs(value);
		StringBuilder builder = new StringBuilder();
		while (value > 0) {
			for (Object[] pair : num_to_roman) {
				if (value - (int) pair[0] >= 0) {
					builder.append((String) pair[1]);
					value -= (int) pair[0];
					break;
				}
			}
		}
		return sign + builder;
	}

	public static long romanNumeralsToNumber(String value) throws NumberFormatException{
		String working = value.toUpperCase().trim().replace("_", "");
		if (!working.matches("[+\\-]?[IVXLCDM]*")) {
			throw new NumberFormatException("Invalid Roman numerals: " + value);
		}
		long parsed = 0;
		long sign = 1;
		if (working.charAt(0) == '-' || working.charAt(0) == '+') {
			sign = working.charAt(0) == '-' ? -1 : 1;
			working = working.substring(1);
		}
		if (working.isEmpty()) {
			return 0;
		}
		while (!working.isEmpty()) {
			for (Object[] pair : roman_to_num) {
				if (working.startsWith((String) pair[1])) {
					parsed += (int) pair[0];
					working = working.substring(((String) pair[1]).length());
					break;
				}
			}
		}
		return parsed * sign;
	}
}
