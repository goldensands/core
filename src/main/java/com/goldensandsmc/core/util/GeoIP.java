package com.goldensandsmc.core.util;

import com.goldensandsmc.core.SandcastleCore;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Scanner;

/**
 * @author ShortCircuit908
 *         Created on 6/28/2017.
 */
public class GeoIP {
	private InetAddress address;
	private String country_code;
	private String country_name;
	private String region_code;
	private String region_name;
	private String city;
	private String zip_code;
	private String time_zone;
	private double latitude;
	private double longitude;
	private int metro_code;

	public InetAddress getAddress() {
		return address;
	}

	public String getCountryCode() {
		return country_code;
	}

	public String getCountryName() {
		return country_name;
	}

	public String getRegionCode() {
		return region_code;
	}

	public String getRegionName() {
		return region_name;
	}

	public String getCity() {
		return city;
	}

	public String getZipCode() {
		return zip_code;
	}

	public String getTimeZone() {
		return time_zone;
	}

	public double getLatitude() {
		return latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public int getMetroCode() {
		return metro_code;
	}

	public boolean exists(){
		return (country_code != null && !country_code.isEmpty())
				|| (country_name != null && !country_name.isEmpty())
				|| (region_code != null && !region_code.isEmpty())
				|| (region_name != null && !region_name.isEmpty())
				|| (city != null && !city.isEmpty())
				|| (zip_code != null && !zip_code.isEmpty())
				|| (time_zone != null && !time_zone.isEmpty());
	}

	public static GeoIP getGeoIPInformation(InetAddress address) throws IOException {
		HttpURLConnection connection = (HttpURLConnection) new URL("https://freegeoip.net/json/"
				+ URLEncoder.encode(address.getHostAddress(), "UTF-8")).openConnection();
		connection.setRequestProperty("User-Agent", SandcastleCore.getInstance().getName());
		try (InputStream in = connection.getInputStream()) {
			try (Scanner scanner = new Scanner(in).useDelimiter("\\A")) {
				if (scanner.hasNext()) {
					return JsonUtils.fromJson(scanner.next(), GeoIP.class);
				}
				return null;
			}
		}
	}
}
