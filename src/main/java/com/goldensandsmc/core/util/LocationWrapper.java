package com.goldensandsmc.core.util;

import com.goldensandsmc.core.SandcastleCore;
import com.goldensandsmc.core.exception.NoSuchWorldException;
import org.bukkit.Location;
import org.bukkit.World;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

/**
 * @author ShortCircuit908
 *         Created on 6/7/2017.
 */
public class LocationWrapper {
	private int stored_id;
	private transient Location location;
	private final int server_id;
	private final UUID world_id;
	private final double x;
	private final double y;
	private final double z;
	private final float yaw;
	private final float pitch;

	public LocationWrapper(Location location) {
		this(SandcastleCore.getInstance().getServerId(), location);
	}

	public LocationWrapper(int server_id, Location location) {
		this(-1, server_id, location);
	}

	private LocationWrapper(int stored_id, int server_id, Location location) {
		this(stored_id, server_id, location.getWorld().getUID(), location.getX(), location.getY(), location.getZ(), location.getYaw(), location.getPitch());
	}

	private LocationWrapper(int stored_id, UUID world_id, double x, double y, double z, float yaw, float pitch) {
		this(stored_id, SandcastleCore.getInstance().getServerId(), world_id, x, y, z, yaw, pitch);
	}

	public LocationWrapper(UUID world_id, double x, double y, double z, float yaw, float pitch) {
		this(-1, SandcastleCore.getInstance().getServerId(), world_id, x, y, z, yaw, pitch);
	}

	public LocationWrapper(int stored_id, int server_id, UUID world_id, double x, double y, double z, float yaw, float pitch) {
		this.stored_id = stored_id;
		this.server_id = server_id;
		this.world_id = world_id;
		this.x = x;
		this.y = y;
		this.z = z;
		this.yaw = yaw;
		this.pitch = pitch;
	}

	public Location toLocation() throws NoSuchWorldException {
		if (location == null) {
			World world = SandcastleCore.getInstance().getServer().getWorld(world_id);
			if (world == null) {
				throw new NoSuchWorldException();
			}
			location = new Location(world, x, y, z, yaw, pitch);
		}
		return location;
	}

	public int getServerId() {
		return server_id;
	}

	public int getStoredId() {
		return stored_id;
	}

	public boolean isStored() {
		return stored_id > -1;
	}

	public int save() throws SQLException {
		try (Connection connection = SandcastleCore.getInstance().getDBHandler().getConnection()) {
			return save(connection);
		}
	}

	public int save(Connection connection) throws SQLException {
		if (isStored()) {
			return stored_id;
		}
		try (PreparedStatement statement = connection.prepareStatement("INSERT INTO `locations` (`server_id`, `world_id`, `x`, `y`, `z`, `yaw`, `pitch`) VALUES (?, UNHEX(?), ?, ?, ?, ?, ?)")) {
			statement.setInt(1, server_id);
			statement.setString(2, Utils.stringFromUuid(world_id));
			statement.setDouble(3, x);
			statement.setDouble(4, y);
			statement.setDouble(5, z);
			statement.setFloat(6, yaw);
			statement.setFloat(7, pitch);
			statement.execute();
		}
		return stored_id = Utils.getLastInsertId(connection);
	}

	public void delete() throws SQLException {
		try (Connection connection = SandcastleCore.getInstance().getDBHandler().getConnection()) {
			delete(connection);
		}
	}

	public void delete(Connection connection) throws SQLException {
		if (!isStored()) {
			return;
		}
		deleteLocation(stored_id, connection);
		stored_id = -1;
	}

	public static void deleteLocation(int id) throws SQLException {
		try (Connection connection = SandcastleCore.getInstance().getDBHandler().getConnection()) {
			deleteLocation(id, connection);
		}
	}

	public static void deleteLocation(int id, Connection connection) throws SQLException {
		try (PreparedStatement statement = connection.prepareStatement("DELETE FROM `locations` WHERE `id`=?")) {
			statement.setInt(1, id);
			statement.execute();
		}
	}

	public static LocationWrapper fetchLocation(int id) throws SQLException {
		try (Connection connection = SandcastleCore.getInstance().getDBHandler().getConnection()) {
			return fetchLocation(id, connection);
		}
	}

	public static LocationWrapper fetchLocation(int id, Connection connection) throws SQLException {
		try (PreparedStatement statement = connection.prepareStatement("SELECT `id`, `server_id`, HEX(`world_id`), `x`, `y`, `z`, `yaw`, `pitch` FROM `locations` WHERE `id`=?")) {
			statement.setInt(1, id);
			try (ResultSet result = statement.executeQuery()) {
				if (result.next()) {
					return new LocationWrapper(
							result.getInt(1),
							result.getInt(2),
							Utils.uuidFromString(result.getString(3)),
							result.getDouble(4),
							result.getDouble(5),
							result.getDouble(6),
							result.getFloat(7),
							result.getFloat(8)
					);
				}
				return null;
			}
		}
	}
}
