package com.goldensandsmc.core.util;

import org.bukkit.Bukkit;
import org.bukkit.enchantments.Enchantment;

import java.lang.reflect.Field;
import java.util.LinkedList;

/**
 * @author ShortCircuit908
 *         Created on 6/8/2017.
 */
@SuppressWarnings("unchecked")
public class EnchantmentHelper {
	private static final Field enchantment_acceptingNew;
	//private static final Field enchantcommand_ENCHANTMENT_NAMES;

	static {
		Field temp = null;
		try {
			temp = Enchantment.class.getDeclaredField("acceptingNew");
		}
		catch (ReflectiveOperationException e) {
			e.printStackTrace();
		}
		enchantment_acceptingNew = temp;
		/*
		temp = null;
		try {
			temp = EnchantCommand.class.getDeclaredField("ENCHANTMENT_NAMES");
		}
		catch (ReflectiveOperationException e) {
			e.printStackTrace();
		}
		enchantcommand_ENCHANTMENT_NAMES = temp;
		*/
	}

	public static boolean registerCustomEnchantment(CustomEnchantment enchantment) {
		if (!trySetAcceptingNew()) {
			return false;
		}
		Bukkit.getLogger().info("Registering enchantment: " + enchantment.getName());
		try {
			Enchantment.registerEnchantment(enchantment);
		}
		catch (IllegalArgumentException e) {
			Bukkit.getLogger().info("Enchantment already exists: " + enchantment.getName());
			return false;
		}
		//tryRebuildEnchantments();
		return true;
	}

	private static boolean trySetAcceptingNew() {
		try {
			enchantment_acceptingNew.setAccessible(true);
			enchantment_acceptingNew.set(null, true);
			return true;
		}
		catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		return false;
	}

	/*
	private static boolean tryRebuildEnchantments() {
		try {
			enchantcommand_ENCHANTMENT_NAMES.setAccessible(true);
			List<String> enchantment_names = (List<String>) enchantcommand_ENCHANTMENT_NAMES.get(null);
			enchantment_names.clear();
			EnchantCommand.buildEnchantments();
			return true;
		}
		catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		return false;
	}
	*/

	public static Enchantment[] getAllEnchantments() {
		return Enchantment.values();
	}

	public static Enchantment[] getCustomEnchantments() {
		LinkedList<Enchantment> custom_enchantments = new LinkedList<>();
		for (Enchantment enchantment : getAllEnchantments()) {
			if (enchantment instanceof CustomEnchantment) {
				custom_enchantments.add(enchantment);
			}
		}
		return custom_enchantments.toArray(new Enchantment[0]);
	}

	public static Enchantment getEnchantment(String name) {
		for (Enchantment enchantment : getAllEnchantments()) {
			if (enchantment.getName().equalsIgnoreCase(name)) {
				return enchantment;
			}
		}
		return null;
	}

	public static Enchantment getEnchantment(int id) {
		for (Enchantment enchantment : getAllEnchantments()) {
			if (enchantment.getId() == id) {
				return enchantment;
			}
		}
		return null;
	}
}
