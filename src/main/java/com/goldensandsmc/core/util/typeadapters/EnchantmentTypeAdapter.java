package com.goldensandsmc.core.util.typeadapters;

import com.goldensandsmc.core.util.EnchantmentHelper;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import java.io.IOException;
import org.bukkit.enchantments.Enchantment;

/**
 * @author ShortCircuit908
 *         Created on 7/6/2017.
 */
public class EnchantmentTypeAdapter extends TypeAdapter<Enchantment> {

	public EnchantmentTypeAdapter() {
	}

	@Override
	public void write(JsonWriter out, Enchantment enchantment) throws IOException {
		if (enchantment == null) {
			out.nullValue();
			return;
		}
		out.value(enchantment.getName());
	}

	@Override
	public Enchantment read(JsonReader in) throws IOException {
		if (in.peek() == JsonToken.NULL) {
			in.nextNull();
			return null;
		}
		return EnchantmentHelper.getEnchantment(in.nextString());
	}
}
