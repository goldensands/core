package com.goldensandsmc.core.util.typeadapters;

import com.goldensandsmc.core.util.bukkitreflect.ChatComponentHelper;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import java.io.IOException;

/**
 * @author ShortCircuit908
 *         Created on 7/6/2017.
 */
public class IChatBaseComponentTypeAdapter extends TypeAdapter {

	public IChatBaseComponentTypeAdapter() {

	}

	@Override
	public void write(JsonWriter out, Object value) throws IOException {
		out.value(ChatComponentHelper.stringFromChatComponent(value));
	}

	@Override
	public Object read(JsonReader in) throws IOException {
		return ChatComponentHelper.chatComponentFromString(in.nextString());
	}
}
