package com.goldensandsmc.core.util.typeadapters;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import java.util.Map;
import org.bukkit.inventory.meta.EnchantmentStorageMeta;
import org.bukkit.inventory.meta.ItemMeta;

/**
 * @author ShortCircuit908
 *         Created on 7/6/2017.
 */
public class EnchantmentStorageMetaExclusionStrategy implements ExclusionStrategy {

	public EnchantmentStorageMetaExclusionStrategy() {

	}

	@Override
	public boolean shouldSkipField(FieldAttributes f) {
		if (!ItemMeta.class.isAssignableFrom(f.getDeclaringClass()) || !f.getName().equals("enchantments") || !Map.class.isAssignableFrom(f.getDeclaredClass())) {
			return false;
		}
		return !EnchantmentStorageMeta.class.isAssignableFrom(f.getDeclaringClass());
	}

	@Override
	public boolean shouldSkipClass(Class<?> clazz) {
		return false;
	}
}
