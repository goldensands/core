package com.goldensandsmc.core.util.typeadapters;

import com.goldensandsmc.core.util.ReflectionHelper;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.UUID;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;

/**
 * @author ShortCircuit908
 *         Created on 7/6/2017.
 */
public class GameProfileTypeAdapter extends TypeAdapter {

	public GameProfileTypeAdapter() {

	}

	@Override
	public void write(JsonWriter out, Object value) throws IOException {
		if (value == null) {
			out.nullValue();
			return;
		}
		UUID id = null;
		String name = null;
		try {
			Field field_id = ReflectionHelper.getField(value.getClass(), "id");
			field_id.setAccessible(true);
			Field field_name = ReflectionHelper.getField(value.getClass(), "name");
			field_name.setAccessible(true);
			id = (UUID) field_id.get(value);
			name = (String) field_name.get(value);
		}
		catch (ReflectiveOperationException e) {
			e.printStackTrace();
		}
		out.beginObject();
		if (id != null) {
			out.name("id");
			ItemStackTypeAdapter.GSON.toJson(id, UUID.class, out);
		}
		if (name != null) {
			out.name("name").value(name);
		}
		out.endObject();
	}

	@Override
	public Object read(JsonReader in) throws IOException {
		if (in.peek() == JsonToken.NULL) {
			in.nextNull();
			return null;
		}
		UUID id = null;
		String name = null;
		in.beginObject();
		while (in.hasNext()) {
			switch (in.nextName().toLowerCase()) {
				case "id":
				case "uuid":
					id = ItemStackTypeAdapter.GSON.fromJson(in, UUID.class);
					break;
				case "name":
				case "username":
					name = in.nextString();
					break;
			}
		}
		in.endObject();
		if (id == null && name == null) {
			return null;
		}
		if (id == null) {
			OfflinePlayer player = Bukkit.getServer().getOfflinePlayer(name);
			if(player.hasPlayedBefore()) {
				id = player.getUniqueId();
			}
		}
		if (name == null) {
			OfflinePlayer player = Bukkit.getServer().getOfflinePlayer(id);
			if(player.hasPlayedBefore()) {
				name = player.getName();
			}
		}
		try {
			Constructor<?> constructor_gameprofile = ReflectionHelper.getConstructor(getGameProfileClass(), UUID.class, String.class);
			return constructor_gameprofile.newInstance(id, name);
		}
		catch (ReflectiveOperationException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static Class<?> getGameProfileClass() throws ClassNotFoundException {
		try {
			return Class.forName("net.minecraft.util.com.mojang.authlib.GameProfile");
		}
		catch (ClassNotFoundException e) {
			return Class.forName("com.mojang.authlib.GameProfile");
		}
	}
}
