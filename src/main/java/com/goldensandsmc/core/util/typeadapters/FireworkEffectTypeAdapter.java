package com.goldensandsmc.core.util.typeadapters;

import com.google.gson.TypeAdapter;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.LinkedList;
import java.util.List;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;

/**
 * @author ShortCircuit908
 *         Created on 7/6/2017.
 */
public class FireworkEffectTypeAdapter extends TypeAdapter<FireworkEffect> {

	public FireworkEffectTypeAdapter() {

	}

	@Override
	public void write(JsonWriter out, FireworkEffect value) throws IOException {
		if (value == null) {
			out.nullValue();
			return;
		}
		Type color_list_type = new TypeToken<List<Color>>() {
		}.getType();
		out.beginObject();
		out.name("type").value(value.getType().name());
		if (value.getColors() != null && !value.getColors().isEmpty()) {
			out.name("colors");
			ItemStackTypeAdapter.GSON.toJson(value.getColors(), color_list_type, out);
		}
		if (value.getFadeColors() != null && !value.getFadeColors().isEmpty()) {
			out.name("fadeColors");
			ItemStackTypeAdapter.GSON.toJson(value.getColors(), color_list_type, out);
		}
		if (value.hasFlicker()) {
			out.name("flicker").value(true);
		}
		if (value.hasTrail()) {
			out.name("trail").value(true);
		}
		out.endObject();
	}

	@Override
	public FireworkEffect read(JsonReader in) throws IOException {
		if (in.peek() == JsonToken.NULL) {
			in.nextNull();
			return null;
		}
		Type color_list_type = new TypeToken<LinkedList<Color>>() {
		}.getType();
		FireworkEffect.Type type = null;
		List<Color> colors = null;
		List<Color> fade_colors = null;
		boolean flicker = false;
		boolean trail = false;
		in.beginObject();
		while (in.hasNext()) {
			switch (in.nextName().toLowerCase()) {
				case "type":
				case "shape":
					type = FireworkEffect.Type.valueOf(in.nextString().toUpperCase());
					break;
				case "colors":
					colors = ItemStackTypeAdapter.GSON.fromJson(in, color_list_type);
					break;
				case "fade":
				case "fade-colors":
				case "fade_colors":
				case "fadecolors":
					fade_colors = ItemStackTypeAdapter.GSON.fromJson(in, color_list_type);
					break;
				case "flicker":
					flicker = in.nextBoolean();
					break;
				case "trail":
					trail = in.nextBoolean();
					break;
			}
		}
		in.endObject();
		if (type == null) {
			type = FireworkEffect.Type.BALL;
		}
		if (colors == null) {
			colors = new LinkedList<>();
		}
		if (fade_colors == null) {
			fade_colors = new LinkedList<>();
		}
		return FireworkEffect.builder()
				.with(type)
				.withColor(colors)
				.withFade(fade_colors)
				.flicker(flicker)
				.trail(trail)
				.build();
	}
}
