package com.goldensandsmc.core.util.typeadapters;

import com.goldensandsmc.core.util.ReflectionHelper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Map;
import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 * @author ShortCircuit908
 *         Created on 6/27/2017.
 */
public class ItemStackTypeAdapter extends TypeAdapter<ItemStack> {
	protected static final Gson GSON;

	static {
		GSON = gsonBuilderWithItemSerializers().create();
	}

	public static GsonBuilder gsonBuilderWithItemSerializers() {
		GsonBuilder builder = new GsonBuilder();
		builder.enableComplexMapKeySerialization();
		builder.addSerializationExclusionStrategy(new EnchantmentStorageMetaExclusionStrategy());
		builder.addDeserializationExclusionStrategy(new EnchantmentStorageMetaExclusionStrategy());
		try {
			builder.registerTypeAdapter(ReflectionHelper.getNMSClass("IChatBaseComponent"), new IChatBaseComponentTypeAdapter());
		}
		catch (ReflectiveOperationException e) {
			e.printStackTrace();
		}
		try {
			builder.registerTypeAdapter(ReflectionHelper.getNMSClass("ItemStack"), new ItemStackTypeAdapter());
		}
		catch (ReflectiveOperationException e) {
			e.printStackTrace();
		}
		try {
			builder.registerTypeAdapter(ReflectionHelper.getCraftbukkitClass("inventory.CraftItemStack"), new ItemStackTypeAdapter());
		}
		catch (ReflectiveOperationException e) {
			e.printStackTrace();
		}
		try {
			builder.registerTypeAdapter(ReflectionHelper.getNMSClass("Enchantment"), new EnchantmentTypeAdapter());
		}
		catch (ReflectiveOperationException e) {
			e.printStackTrace();
		}
		try {
			builder.registerTypeAdapter(GameProfileTypeAdapter.getGameProfileClass(), new GameProfileTypeAdapter());
		}
		catch (ReflectiveOperationException e) {
			e.printStackTrace();
		}
		builder.registerTypeAdapter(Color.class, new ColorTypeAdapter());
		builder.registerTypeAdapter(Enchantment.class, new EnchantmentTypeAdapter());
		builder.registerTypeAdapter(FireworkEffect.class, new FireworkEffectTypeAdapter());
		builder.registerTypeAdapter(ItemStack.class, new ItemStackTypeAdapter());
		return builder;
	}

	@Override
	public void write(JsonWriter out, ItemStack value) throws IOException {
		if (value == null) {
			out.nullValue();
			return;
		}
		out.beginObject();
		out.name("type").value(value.getType().name());
		out.name("amount").value(value.getAmount());
		out.name("durability").value(value.getDurability());
		if (value.hasItemMeta()) {
			ItemMeta meta = value.getItemMeta();
			out.name("meta");
			GSON.toJson(meta, meta.getClass(), out);
		}
		out.endObject();
	}

	@Override
	public ItemStack read(JsonReader in) throws IOException {
		if (in.peek() == JsonToken.NULL) {
			in.nextNull();
			return null;
		}
		Material type = null;
		int amount = 0;
		short durability = 0;
		JsonElement raw_meta = null;
		in.beginObject();
		while (in.hasNext()) {
			switch (in.nextName()) {
				case "id":
					int id = in.nextInt();
					for (Material material : Material.values()) {
						if (material.getId() == id) {
							type = material;
							break;
						}
					}
					break;
				case "type":
					try {
						type = Material.valueOf(in.nextString());
					}
					catch (IllegalArgumentException e) {
						// Do nothing
					}
					break;
				case "data":
				case "damage":
				case "durability":
					durability = (short) in.nextInt();
					break;
				case "count":
				case "amount":
					amount = in.nextInt();
					break;
				case "meta":
				case "metadata":
					raw_meta = GSON.fromJson(in, JsonElement.class);
					break;
			}
		}
		in.endObject();
		ItemStack item = new ItemStack(type, amount, durability);
		item.setItemMeta(deserializeMeta(raw_meta, type));
		return item;
	}

	@SuppressWarnings("unchecked")
	public static <T extends ItemMeta> T deserializeMeta(JsonElement element, Material type) {
		if (element == null || !(element instanceof JsonObject)) {
			return null;
		}
		if (type == null) {
			type = Material.STONE;
		}
		ItemMeta dummy_meta = Bukkit.getItemFactory().getItemMeta(type);
		/*
		TypeAdapter adapter = GSON.getAdapter(dummy_meta.getClass());
		if (adapter instanceof ItemMetaTypeAdapter) {
			ItemMeta instance = ((ItemMetaTypeAdapter) adapter).newMetaInstance();
			((ItemMetaTypeAdapter) adapter).readBase((JsonObject) element, instance);
			((ItemMetaTypeAdapter) adapter).readExtra((JsonObject) element, instance);
			return (T) instance;
		}
		*/
		for (Map.Entry<String, JsonElement> entry : ((JsonObject) element).entrySet()) {
			try {
				Field field = ReflectionHelper.getField(dummy_meta.getClass(), entry.getKey());
				field.setAccessible(true);
				field.set(dummy_meta, GSON.fromJson(entry.getValue(), field.getGenericType()));
			}
			catch (NoSuchFieldException e) {
				// Do nothing
			}
			catch (ReflectiveOperationException e) {
				e.printStackTrace();
			}
		}
		return (T) dummy_meta;
	}

	public static JsonElement serializeMeta(ItemMeta meta) {
		if (meta == null) {
			return JsonNull.INSTANCE;
		}
		return GSON.toJsonTree(meta, meta.getClass());
	}
}
