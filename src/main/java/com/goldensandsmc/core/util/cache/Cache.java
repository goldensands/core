package com.goldensandsmc.core.util.cache;

import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * @author ShortCircuit908
 *         Created on 5/18/2017.
 */
public class Cache<T> {
	private static final Timer expiry_timer = new Timer();

	private final LinkedList<CachedValue<T>> values = new LinkedList<>();
	private TimerTask expiry_check_task;
	private ExpiryType expiry_type;
	private long expire_millis;
	private ExpireTask<T> expire_task;


	public Cache(long expire, TimeUnit unit, ExpiryType expiry_type) {
		setExpireTime(expire, unit);
		setExpiryType(expiry_type);
	}

	public void setExpireTime(long expire, TimeUnit unit) {
		this.expire_millis = TimeUnit.MILLISECONDS.convert(expire, unit);
		if (this.expiry_check_task != null) {
			this.expiry_check_task.cancel();
		}
		this.expiry_check_task = new TimerTask() {
			@Override
			public void run() {
				expireEntries();
			}
		};
		expiry_timer.schedule(this.expiry_check_task, 0, this.expire_millis);
	}

	public void setExpiryType(ExpiryType expiry_type) {
		this.expiry_type = expiry_type;
	}

	public void setExpireTask(ExpireTask<T> expire_task) {
		this.expire_task = expire_task;
	}

	public Collection<CachedValue<T>> getValues() {
		return this.values;
	}

	protected Iterator<CachedValue<T>> iterator() {
		return this.values.iterator();
	}

	public void expireEntries() {
		expireEntries(System.currentTimeMillis(), this.iterator());
	}

	protected void expireEntries(long now, Iterator<CachedValue<T>> iterator) {
		while (iterator.hasNext()) {
			CachedValue<T> value = iterator.next();
			if (!value.canExpire()) {
				continue;
			}
			switch (this.expiry_type) {
				case SINCE_CREATED:
					if (value.getTimeCreated() + this.expire_millis <= now) {
						value.expire();
					}
					break;
				case SINCE_ACCESSED:
					if (value.getTimeLastAccessed() + this.expire_millis <= now) {
						value.expire();
					}
					break;
			}
			if (value.isExpired()) {
				iterator.remove();
				if (this.expire_task != null) {
					this.expire_task.onValueExpired(value);
				}
			}
		}
	}

	public interface ExpireTask<T> {
		void onValueExpired(CachedValue<T> value);
	}
}
