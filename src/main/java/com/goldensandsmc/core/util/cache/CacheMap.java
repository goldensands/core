package com.goldensandsmc.core.util.cache;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

/**
 * @author ShortCircuit908
 *         Created on 5/18/2017.
 */
public class CacheMap<K, V> extends Cache<V> {
	private final HashMap<K, CachedValue<V>> values = new HashMap<>();

	public CacheMap(long expire, TimeUnit unit, ExpiryType expiry_type) {
		super(expire, unit, expiry_type);
	}

	@Override
	public Iterator<CachedValue<V>> iterator() {
		return this.values.values().iterator();
	}

	@Override
	public Collection<CachedValue<V>> getValues() {
		return this.values.values();
	}

	public CachedValue<V> get(K key) {
		return this.values.get(key);
	}

	public CachedValue<V> put(K key, V value) {
		if (this.values.containsKey(key)) {
			CachedValue<V> cached = this.values.get(key);
			cached.setValue(value);
			return cached;
		}
		CachedValue<V> cached = new CachedValue<>(value);
		this.values.put(key, cached);
		return cached;
	}

	public boolean containsKey(K key) {
		return this.values.containsKey(key);
	}

	public boolean containsValue(V value) {
		return this.values.containsValue(new CachedValue<>(value));
	}

	public CachedValue<V> remove(K key) {
		return this.values.remove(key);
	}

	public boolean remove(K key, V value) {
		return this.values.remove(key, value);
	}
}
