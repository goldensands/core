package com.goldensandsmc.core.util;

import com.google.common.base.Joiner;
import com.google.gson.JsonParseException;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang.StringEscapeUtils;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.meta.BookMeta;
import org.bukkit.inventory.meta.ItemMeta;

/**
 * @author ShortCircuit908
 *         Created on 6/19/2017.
 */
public class ReflectionParser {
	private static final Pattern single_pair_pattern = Pattern.compile("(\\w+)\\s?[:=]\\s?(([^;])*);?");
	private static final Pattern single_value_pattern = Pattern.compile("\"[^\"]*\"|'[^']*'|[^\\s\"']+");

	private final Object object;
	private final String meta_string;
	private final HashMap<String, LinkedList<String>> parsed_values = new HashMap<>();

	public ReflectionParser(Object object, String meta_string) {
		this.meta_string = meta_string;
		this.object = object;
	}

	public ReflectionParser parse() {
		parsed_values.clear();
		StringBuilder tmp_str = new StringBuilder(meta_string);
		Matcher matcher;
		Matcher value_matcher;
		while ((matcher = single_pair_pattern.matcher(tmp_str.toString())).find()) {
			tmp_str.delete(matcher.start(), matcher.end());
			String property_name = matcher.group(1);
			StringBuilder values_string = new StringBuilder(matcher.group(2));
			LinkedList<String> values = new LinkedList<>();
			while (values_string.length() > 0 && (value_matcher = single_value_pattern.matcher(values_string.toString())).find()) {
				String value = value_matcher.group(0);
				values_string.delete(value_matcher.start(), value_matcher.end());
				values.add(ChatColor.translateAlternateColorCodes('&', StringEscapeUtils.unescapeJava(value)));
			}
			parsed_values.put(property_name, values);
		}
		return this;
	}

	public void apply() {
		apply(null);
	}

	public void apply(Player player) throws JsonParseException {
		LinkedList<String> unknown_fields = new LinkedList<>();
		for (Map.Entry<String, LinkedList<String>> parsed_value : parsed_values.entrySet()) {
			Field matching_field = null;
			for (Field field : ReflectionHelper.getAllFields(object.getClass())) {
				if (field.getName().equals(parsed_value.getKey())) {
					matching_field = field;
					break;
				}
			}
			if (matching_field == null) {
				unknown_fields.add(parsed_value.getKey());
				continue;
			}
			matching_field.setAccessible(true);
			if (Modifier.isStatic(matching_field.getModifiers())) {
				unknown_fields.add(parsed_value.getKey());
				continue;
			}
			try {
				// Special handling for stuff
				if (object instanceof ItemMeta && parsed_value.getKey().equals("lore")) {
					LinkedList<String> copy = new LinkedList<>();
					for (String value : parsed_value.getValue()) {
						if (value.matches("'[^']*'|\"[^\"]*\"")) {
							value = value.substring(1, value.length() - 1);
						}
						copy.add(value);
					}
					((ItemMeta) object).setLore(copy);
				}
				else if (object instanceof BookMeta && parsed_value.getKey().equals("pages")) {
					LinkedList<String> copy = new LinkedList<>();
					for (String value : parsed_value.getValue()) {
						if (value.matches("'[^']*'|\"[^\"]*\"")) {
							value = value.substring(1, value.length() - 1);
						}
						copy.add(value);
					}
					((BookMeta) object).setPages(copy);
				}
				else {
					Object converted = convert(parsed_value.getValue(), matching_field.getType());
					matching_field.set(object, converted);
				}
			}
			catch (IllegalAccessException e) {
				e.printStackTrace();
				unknown_fields.add(parsed_value.getKey());
			}
		}
		if (player != null && !unknown_fields.isEmpty()) {
			player.sendMessage(ChatColor.RED + "The following fields could not be set: " + Joiner.on(", ").join(unknown_fields));
		}
	}

	private static Object convert(LinkedList<String> values, Class<?> convert_class) {
		if (convert_class.isArray() || Collection.class.isAssignableFrom(convert_class) || Map.class.isAssignableFrom(convert_class)) {
			return JsonUtils.fromJson(JsonUtils.toJson(values), convert_class);
		}
		if (values.isEmpty()) {
			return null;
		}
		return JsonUtils.fromJson(values.get(0), convert_class);
	}
}
