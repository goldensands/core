package com.shortcircuit.utils;

/**
 * An object to support a thread-blocking callback
 * <p/>
 * Once {@link #getValue()} is called, the thread which calls the method will block until
 * {@link #setValue(Object)} is called from another thread
 *
 * @author ShortCircuit908
 *         Created on 9/30/2015
 */
public class BlockingCallback<T> {
	private final Object lock = new Object();
	private boolean value_set = false;
	private T value = null;

	public BlockingCallback() {
		this(null);
	}

	public BlockingCallback(T default_value) {
		value = default_value;
	}

	public void setValue(T value) {
		this.value = value;
		value_set = true;
		synchronized (lock) {
			lock.notify();
		}
	}

	public T getValue() {
		if (!value_set) {
			synchronized (lock) {
				try {
					lock.wait();
				}
				catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		return value;
	}
}
