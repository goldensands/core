package com.shortcircuit.utils.bukkit;

import com.shortcircuit.utils.Version;
import com.shortcircuit.utils.bukkit.command.PermissionUtils;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.Plugin;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

/**
 * @author ShortCircuit908
 *         Created on 11/2/2015
 */
public class SpigotPluginUpdater implements Runnable, Listener {
	private static final String query_url = "http://www.spigotmc.org/api/general.php";
	private final Plugin plugin;
	private final int resource_id;
	private boolean update_failed = false;
	private Version latest_version;

	public SpigotPluginUpdater(Plugin plugin, int resource_id) {
		this.plugin = plugin;
		this.resource_id = resource_id;
		plugin.getServer().getScheduler().scheduleAsyncDelayedTask(plugin, this, 0);
	}

	public static Version query(Plugin plugin, int resource_id) throws IOException {
		try {
			plugin.getLogger().info("Checking Spigot for updates...");
			URL url = new URL(query_url);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setDoOutput(true);
			connection.setRequestMethod("POST");
			connection.getOutputStream().write(
					("key=98BE0FE67F88AB82B4C197FAF1DC3B69206EFDCC4D3B80FC83A00037510B99B4&resource=" + resource_id).getBytes("UTF-8"));
			StringBuilder builder = new StringBuilder();
			Scanner scanner = new Scanner(connection.getInputStream());
			while (scanner.hasNextLine()) {
				builder.append(scanner.nextLine());
			}
			Version remote_version = new Version(builder.toString());
			Version local_version = new Version(plugin.getDescription().getVersion());
			int diff = local_version.compareTo(remote_version);
			if (diff < 0) {
				plugin.getLogger().info("An update is available: " + remote_version);
				plugin.getLogger().info("Download the update at " + (plugin.getDescription().getWebsite() == null
						? "https://www.spigotmc.org/resources/" + resource_id : plugin.getDescription().getWebsite()));
				return remote_version;
			}
			if (diff > 0) {
				plugin.getLogger().info("This plugin's version is newer than any public releases");
				plugin.getLogger().info("Are you a developer, or a time traveler?");
				return null;
			}
			plugin.getLogger().info("This plugin is up-to-date");
			return null;
		}
		catch (IOException e) {
			plugin.getLogger().warning("Failed to check Spigot for updates:");
			plugin.getLogger().warning(e.getClass().getCanonicalName() + ": " + e.getMessage());
			throw e;
		}
	}

	@Override
	public void run() {
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
		try {
			latest_version = query(plugin, resource_id);
		}
		catch (IOException e) {
			update_failed = true;
		}
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void notify(final PlayerJoinEvent event) {
		Player player = event.getPlayer();
		if (latest_version != null && PermissionUtils.hasPermission(player, "updater.notify")) {
			if (update_failed) {
				player.sendMessage(ChatColor.RED + String.format("[%1$s] Failed to check Spigot for updates. Please check " +
						"the console for more information.", plugin.getName()));
				return;
			}
			player.sendMessage(ChatColor.AQUA + String.format("[%1$s] An update has been found: %2$s",
					plugin.getName(), latest_version));
			player.sendMessage(ChatColor.AQUA + "Download the update at " + (plugin.getDescription().getWebsite() == null
					? "https://www.spigotmc.org/resources/" + resource_id : plugin.getDescription().getWebsite()));
		}
	}
}
