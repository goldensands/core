package com.shortcircuit.utils.bukkit.command.exceptions;

/**
 * @author ShortCircuit908
 *         Created on 11/2/2015
 */
public class CommandException extends Exception {

	public CommandException() {

	}

	public CommandException(String message) {
		super(message);
	}

	public CommandException(String message, Throwable cause) {
		super(message, cause);
	}

	public CommandException(Throwable cause) {
		super(cause);
	}
}
