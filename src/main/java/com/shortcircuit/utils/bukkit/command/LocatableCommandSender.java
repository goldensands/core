package com.shortcircuit.utils.bukkit.command;

import org.bukkit.Location;
import org.bukkit.command.BlockCommandSender;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Wrapper for a {@link org.bukkit.command.CommandSender CommandSender} which has a {@link org.bukkit.Location Location} associated with it
 * <p/>
 * Applies to {@link org.bukkit.command.BlockCommandSender BlockCommandSender} and {@link org.bukkit.entity.Player Player}
 *
 * @author ShortCircuit908
 *         Created on 10/30/2015
 */
public class LocatableCommandSender extends CommandSenderWrapper<CommandSender> {

	protected LocatableCommandSender(CommandSender wrap) {
		super(wrap);
		assert wrap instanceof BlockCommandSender || wrap instanceof Player;
	}

	/**
	 * Get the {@link org.bukkit.Location Location} associated with the sender
	 *
	 * @return The sender's location
	 */
	public Location getLocation() {
		if (wrap instanceof BlockCommandSender) {
			return ((BlockCommandSender) wrap).getBlock().getLocation();
		}
		return ((Player) wrap).getLocation();
	}

	/**
	 * Check if the command sender is a {@link org.bukkit.entity.Player Player}
	 *
	 * @return <code>true</code> if the sender is a {@link org.bukkit.entity.Player Player}
	 */
	public boolean isPlayer() {
		return wrap instanceof Player;
	}

	/**
	 * Check if the command sender is a {@link org.bukkit.command.BlockCommandSender BlockCommandSender}
	 *
	 * @return <code>true</code> if the sender is a {@link org.bukkit.command.BlockCommandSender BlockCommandSender}
	 */
	public boolean isBlock() {
		return wrap instanceof BlockCommandSender;
	}

	/**
	 * Cast the wrapped sender to a {@link org.bukkit.entity.Player Player}
	 *
	 * @return The sender as a {@link org.bukkit.entity.Player Player}
	 * @throws ClassCastException
	 */
	public Player asPlayer() throws ClassCastException {
		return (Player) wrap;
	}

	/**
	 * Cast the wrapped sender to a {@link org.bukkit.command.BlockCommandSender BlockCommandSender}
	 *
	 * @return The sender as a {@link org.bukkit.command.BlockCommandSender BlockCommandSender}
	 * @throws ClassCastException
	 */
	public BlockCommandSender asBlock() throws ClassCastException {
		return (BlockCommandSender) wrap;
	}

	@Override
	public boolean equals(Object other) {
		return other instanceof LocatableCommandSender && wrap.equals(((CommandSenderWrapper) other).wrap);
	}
}