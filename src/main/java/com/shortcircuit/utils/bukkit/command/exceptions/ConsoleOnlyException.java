package com.shortcircuit.utils.bukkit.command.exceptions;

/**
 * @author ShortCircuit908
 */
public class ConsoleOnlyException extends CommandException {
	public ConsoleOnlyException() {
		super("This command is console-only");
	}
}
