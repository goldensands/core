package com.shortcircuit.utils.bukkit.command.exceptions;

/**
 * @author ShortCircuit908
 */
public class TooManyArgumentsException extends CommandException {
	public TooManyArgumentsException() {
		super("Too many arguments");
	}
}
