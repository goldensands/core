package com.shortcircuit.utils.bukkit.command;

import org.bukkit.Bukkit;
import org.bukkit.permissions.Permissible;
import org.bukkit.permissions.Permission;
import org.bukkit.permissions.PermissionAttachment;
import org.bukkit.plugin.Plugin;

import java.util.List;
import java.util.Map;

/**
 * @author ShortCircuit908
 *         Created on 9/29/2015
 */
public class PermissionUtils {
	public static boolean hasPermission(Permissible user, String permission) {
		if (user.hasPermission(permission)) {
			return true;
		}
		String[] parts = permission.split("\\.");
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < parts.length - 1; i++) {
			builder.append(parts[i]).append('.');
			if (user.hasPermission(builder + "*")) {
				return true;
			}
		}
		return false;
	}

	public static void explodeKnownWildcardPermissions(Permissible user) {
		String name;
		String[] parts;
		String current;
		StringBuilder builder;
		List<Permission> known_permissions;
		PermissionAttachment attachment = null;
		for (Plugin plugin : Bukkit.getServer().getPluginManager().getPlugins()) {
			known_permissions = plugin.getDescription().getPermissions();
			for (Permission known_permission : known_permissions) {
				name = known_permission.getName();
				parts = name.split("\\.");
				builder = new StringBuilder();
				for (String part : parts) {
					builder.append(part).append('.');
					current = builder.toString();
					if (user.hasPermission(current + "*")) {
						for (Map.Entry<String, Boolean> entry : known_permission.getChildren().entrySet()) {
							if (!user.hasPermission(entry.getKey())) {
								if (attachment == null) {
									attachment = user.addAttachment(plugin);
								}
								attachment.setPermission(entry.getKey(), true);
							}
						}
					}
				}
			}
			attachment = null;
		}
	}
}
