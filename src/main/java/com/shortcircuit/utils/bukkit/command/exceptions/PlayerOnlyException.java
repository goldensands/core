package com.shortcircuit.utils.bukkit.command.exceptions;

/**
 * @author ShortCircuit908
 */
public class PlayerOnlyException extends CommandException {
	public PlayerOnlyException() {
		super("This command is player-only");
	}
}
