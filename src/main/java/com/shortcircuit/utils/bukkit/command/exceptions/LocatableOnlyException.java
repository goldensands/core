package com.shortcircuit.utils.bukkit.command.exceptions;

/**
 * @author ShortCircuit908
 */
public class LocatableOnlyException extends CommandException {
	public LocatableOnlyException() {
		super("This command is ingame-only");
	}
}
