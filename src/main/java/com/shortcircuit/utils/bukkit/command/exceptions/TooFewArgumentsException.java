package com.shortcircuit.utils.bukkit.command.exceptions;

/**
 * @author ShortCircuit908
 */
public class TooFewArgumentsException extends CommandException {
	public TooFewArgumentsException() {
		super("Too few arguments");
	}
}
