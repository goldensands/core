package com.shortcircuit.utils.bukkit.command;

import com.google.common.base.Joiner;
import com.shortcircuit.utils.collect.ConcurrentArrayList;
import com.shortcircuit.utils.bukkit.command.exceptions.CommandException;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginIdentifiableCommand;
import org.bukkit.plugin.Plugin;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.List;

/**
 * @author ShortCircuit908
 *         Created on 10/30/2015
 */
public abstract class BaseCommand<T extends CommandSender> extends Command implements PluginIdentifiableCommand {
	private static final Joiner default_joiner = Joiner.on("\n");
	private final Plugin plugin;

	protected BaseCommand(Plugin plugin) {
		super(plugin.getClass().getSimpleName() + ":tmp");
		this.plugin = plugin;
		setName(getCommandName());
		if (getCommandAliases() != null) {
			setAliases(Arrays.asList(getCommandAliases()));
		}
		if (getCommandDescription() != null) {
			setDescription(alternateLines(getCommandDescription()));
		}
		if (getCommandUsage() != null) {
			setUsage(alternateLines(getCommandUsage()));
		}
		setPermission(getCommandPermission());
		if (getCommandPermissionMessage() != null) {
			setPermissionMessage(default_joiner.join(getCommandPermissionMessage()));
		}
	}

	@Override
	public final boolean testPermissionSilent(CommandSender target) {
		return PermissionUtils.hasPermission(target, getPermission());
	}

	public abstract String getCommandName();

	public abstract String[] getCommandAliases();

	public abstract CommandType getCommandType();

	public abstract String[] getCommandDescription();

	public abstract String[] getCommandUsage();

	public abstract String getCommandPermission();

	public String[] getCommandPermissionMessage() {
		return null;
	}

	private String alternateLines(String[] lines) {
		boolean color = false;
		StringBuilder builder = new StringBuilder();
		for (String line : lines) {
			builder.append(color ? ChatColor.GRAY : ChatColor.WHITE).append(line).append("\n");
			color ^= true;
		}
		return builder.toString().trim();
	}

	public abstract String[] exec(final T sender, final String command, final ConcurrentArrayList<String> args) throws CommandException;

	@Override
	public final boolean execute(CommandSender sender, String command, String[] args) {
		if (getCommandType() == null) {
			sender.sendMessage(ChatColor.RED + "This command is not applicable to any users");
		}
		if (!getCommandType().isSenderApplicable(sender)) {
			sender.sendMessage(ChatColor.RED + "This command is " + getCommandType().getApplicableSenderName() + "-only");
			return false;
		}
		if (getCommandType() == CommandType.LOCATABLE) {
			sender = new LocatableCommandSender(sender);
		}
		if (!testPermission(sender)) {
			return false;
		}
		try {
			String[] result = exec((T) sender, command, new ConcurrentArrayList<>(args));
			if (result != null) {
				for (String message : result) {
					sender.sendMessage(ChatColor.AQUA + message);
				}
			}
		}
		catch (CommandException e) {
			sender.sendMessage(ChatColor.RED + e.getMessage());
		}
		return true;
	}


	@Override
	public Plugin getPlugin() {
		return plugin;
	}

	protected static synchronized boolean checkSubcommand(final String subcommand, ConcurrentArrayList<String> args) {
		String[] path = subcommand.split("\\s");
		if (args.size() < path.length) {
			return false;
		}
		int i;
		for (i = 0; i < path.length; i++) {
			String part = path[i];
			String arg = args.get(i);
			if (!part.matches(arg)) {
				return false;
			}
		}
		List<String> tmp = args.subList(i, args.size());
		args.clear();
		args.addAll(tmp);
		return true;
	}

	public boolean setName(String name) {
		try {
			Method set_name = Command.class.getDeclaredMethod("setName", String.class);
			set_name.invoke(this, name);
			return true;
		}
		catch (ReflectiveOperationException e) {
			try {
				Field name_field = Command.class.getDeclaredField("name");
				Field modifiers_field = Field.class.getDeclaredField("modifiers");
				modifiers_field.setAccessible(true);
				modifiers_field.setInt(name_field, modifiers_field.getInt(name_field) & Modifier.FINAL);
				name_field.setAccessible(true);
				name_field.set(this, name);
				return true;
			}
			catch (ReflectiveOperationException ex) {
				plugin.getLogger().warning("Unable to set command name:");
				plugin.getLogger().warning(ex.getClass().getCanonicalName() + ": " + ex.getMessage());
			}
		}
		return false;
	}
}
