package com.shortcircuit.utils.bukkit.command.exceptions;

/**
 * @author ShortCircuit908
 *         Created on 11/10/2015
 */
public class NotImplementedException extends CommandException {
	public NotImplementedException() {
		super("This command has not been implemented.");
	}
}
