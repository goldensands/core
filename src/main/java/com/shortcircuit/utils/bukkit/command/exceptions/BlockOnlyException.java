package com.shortcircuit.utils.bukkit.command.exceptions;

/**
 * @author ShortCircuit908
 */
public class BlockOnlyException extends CommandException {
	public BlockOnlyException() {
		super("This command is block-only");
	}
}
