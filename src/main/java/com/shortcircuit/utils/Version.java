package com.shortcircuit.utils;

/**
 * @author ShortCircuit908
 *         Created on 10/28/2015
 */
public class Version implements Comparable<Version> {
	private Integer major = null;
	private Integer minor = null;
	private Integer build = null;
	private Version sub_version = null;
	private boolean snapshot = false;

	public Version(int major) {
		this(major, false);
	}

	public Version(int major, Version sub_version) {
		this(major, sub_version, false);
	}

	public Version(int major, boolean snapshot) {
		this(major, null, snapshot);
	}

	public Version(int major, Version sub_version, boolean snapshot) {
		this.major = major;
		this.sub_version = sub_version;
		this.snapshot = snapshot;
	}

	public Version(int major, int minor) {
		this(major, minor, null, false);
	}

	public Version(int major, int minor, Version sub_version) {
		this(major, minor, sub_version, false);
	}


	public Version(int major, int minor, boolean snapshot) {
		this(major, minor, null, snapshot);
	}

	public Version(int major, int minor, Version sub_version, boolean snapshot) {
		this.major = major;
		this.minor = minor;
		this.sub_version = sub_version;
		this.snapshot = snapshot;
	}

	public Version(int major, int minor, int build) {
		this(major, minor, build, null, false);
	}

	public Version(int major, int minor, int build, Version sub_version) {
		this(major, minor, build, sub_version, false);
	}

	public Version(int major, int minor, int build, boolean snapshot) {
		this(major, minor, build, null, snapshot);
	}

	public Version(int major, int minor, int build, Version sub_version, boolean snapshot) {
		this.major = major;
		this.minor = minor;
		this.build = build;
		this.sub_version = sub_version;
		this.snapshot = snapshot;
	}


	public Version(String version) throws NumberFormatException {
		if (version == null) {
			return;
		}
		String[] version_parts = version.toLowerCase().split("\\-");
		if (version_parts.length >= 2) {
			for (int i = 1; i < version_parts.length; i++) {
				String part = version_parts[i];
				if (part.toLowerCase().matches("r\\d(\\.\\d)*")) {
					sub_version = new Version(part.substring(1, part.length()));
				}
				else if (part.equalsIgnoreCase("snapshot")) {
					snapshot = true;
				}
			}
		}
		String[] parts = version_parts[0].split("\\.");
		if (parts.length >= 3) {
			build = Integer.parseInt(parts[2]);
		}
		if (parts.length >= 2) {
			minor = Integer.parseInt(parts[1]);
		}
		if (parts.length >= 1) {
			major = Integer.parseInt(parts[0]);
		}
	}

	public int getMajor() {
		return major == null ? 0 : major;
	}

	public int getMinor() {
		return minor == null ? 0 : minor;
	}

	public int getBuild() {
		return build == null ? 0 : build;
	}

	public Version getSubVersion() {
		return sub_version;
	}

	public boolean isSnapshot() {
		return snapshot;
	}

	@Override
	public String toString() {
		return (major == null ? "" : major) + (minor == null ? "" : (major == null ? "" : ".") + minor)
				+ (build == null ? "" : (minor == null ? "" : ".") + build) + (snapshot ? "-SNAPSHOT" : "")
				+ (sub_version == null ? "" : "-R" + sub_version.toString());
	}

	@Override
	public boolean equals(Object o) {
		return o != null
				&& o instanceof Version
				&& ((Version) o).major.intValue() == major.intValue()
				&& ((Version) o).minor.intValue() == minor.intValue()
				&& ((Version) o).build.intValue() == build.intValue()
				&& ((Version) o).snapshot == snapshot
				&& (((((Version) o).sub_version != null
				&& sub_version != null) && ((Version) o).sub_version.equals(sub_version))
				|| ((Version) o).sub_version == null && sub_version == null);
	}

	@Override
	public int compareTo(Version o) {
		if (o == null) {
			return 1;
		}
		if (o.getMajor() != getMajor()) {
			return getMajor() - o.getMajor();
		}
		if (o.getMinor() != getMinor()) {
			return getMinor() - o.getMinor();
		}
		if (o.getBuild() != getBuild()) {
			return getBuild() - o.getBuild();
		}
		if (o.isSnapshot() != isSnapshot()) {
			return isSnapshot() ? -1 : 1;
		}
		if (o.sub_version == null && sub_version != null) {
			return -1;
		}
		if (o.sub_version != null && sub_version == null) {
			return 1;
		}
		if (o.sub_version != null) {
			return o.sub_version.compareTo(sub_version);
		}
		return 0;
	}
}